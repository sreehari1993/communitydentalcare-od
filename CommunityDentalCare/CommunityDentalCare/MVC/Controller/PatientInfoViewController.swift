//
//  PatientInfoViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: PDViewController {
    
    @IBOutlet weak var labelDate: UILabel!
   // @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
   // @IBOutlet weak var textfieldMiddleInitial : UITextField!
   // @IBOutlet weak var textFieldDateOfBirth: PDTextField!
 //   @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonNext: UIButton! 
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!

    var isNewPatient : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
//        datePicker.maximumDate = Date()
//        textFieldDateOfBirth.inputView = datePicker
//        textFieldDateOfBirth.inputAccessoryView = toolBar
        MonthInputView.addMonthPickerForTextField(textfieldMonth)

        labelDate.text = patient.dateToday
        if patient.firstName != nil{
            textFieldFirstName.text = patient.firstName
            textFieldLastName.text = patient.lastName
        }
        
         }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(withSender sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if invalidDateofBirth {
            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
            self.present(alert, animated: true, completion: nil)
        } else {
            
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.initial = ""//textfieldMiddleInitial.isEmpty ? "" : textfieldMiddleInitial.text!
            patient.dateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            
            func showMoreThanOneUserAlert()  {
                let alertController = UIAlertController(title: "Community Dental Care", message: "More than one user found. Please handover the device to front desk to enter your patient id", preferredStyle: UIAlertControllerStyle.alert)
                let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                    let newPatientStep1VC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationVC") as! VerificationViewController
                    newPatientStep1VC.patient = self.patient
                    self.navigationController?.pushViewController(newPatientStep1VC, animated: true)
                }
                alertController.addAction(alertYesAction)
                self.present(alertController, animated: true, completion: nil)
                
                
            }
            
            func showUnableToFindAlert (){
                let alertController = UIAlertController(title: "Community Dental Care", message: "Unable to find the patient \(textFieldFirstName.text!) \(textFieldLastName.text!) - \(patient.dateOfBirth) mismatch", preferredStyle: UIAlertControllerStyle.alert)
                let alertYesAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) -> Void in
                    showCreateNewPatientAlert()
                }
                alertController.addAction(alertYesAction)
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            func showCreateNewPatientAlert (){
                let alertController = UIAlertController(title: "Community Dental Care", message: "Do you wish to create a new patient", preferredStyle: UIAlertControllerStyle.alert)
                let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                    self.patient.patientDetails = nil
                    gotoPatientSignInForm()
                    
                }
                alertController.addAction(alertYesAction)
                let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                    
                }
                alertController.addAction(alertNoAction)
                self.present(alertController, animated: true, completion: nil)
                
                
            }
            
            func gotoPatientSignInForm() {
                let patientSignIn = patientStoryboard.instantiateViewController(withIdentifier: "PatientRegistrationStep1Vc") as! PatientRegistrationStep1Vc
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
            
            
            
            func APICall() {
                self.activityIndicator.startAnimating()
                self.buttonNext.isUserInteractionEnabled = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy"
                let date = dateFormatter.date(from: patient.dateOfBirth)
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
                manager.responseSerializer.acceptableContentTypes = ["text/html"]
                manager.post("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.string(from: date!)], progress: { (progress) in
                    }, success: { (task, result) in
                        self.buttonNext.isUserInteractionEnabled = true
                        self.activityIndicator.stopAnimating()
                        if self.navigationController?.topViewController == self {
                            let response = result as! [String : AnyObject]
                            if response["status"] as! String == "success"  {
                                let patientDetails = response["patientData"] as! [String: AnyObject]
                                self.patient.patientDetails = PatientDetails(details: patientDetails)
                                self.gotoNextForm(true)
                            } else {
                                if response["status"] as! String == "failed" && (response["message"] as! String).contains("Not connected") {
                                    let alert = Extention.alert((response["message"] as! String).uppercased())
                                    self.present(alert, animated: true, completion: nil)
                                } else if response["status"] as! String == "matching_name"  {
                                    let patientDetails = response["matchingData"] as! [String: String]
                                    let alertController = UIAlertController(title: "Community Dental Care", message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.alert)
                                    let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                        self.textFieldFirstName.text = patientDetails["Fname"]
                                        self.textFieldLastName.text = patientDetails["Lname"]
                                        self.textfieldDate.text = ""
                                        self.textfieldYear.text = ""
                                        self.textfieldMonth.text = ""
                                    }
                                    alertController.addAction(alertOkAction)
                                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                        showUnableToFindAlert()
                                    }
                                    alertController.addAction(alertNoAction)
                                    self.present(alertController, animated: true, completion: nil)
                                } else if response["status"] as! String == "multiple_patient_found" {
                                    showMoreThanOneUserAlert()
                                } else {
                                    self.patient.patientDetails = nil
                                    let alertController = UIAlertController(title: "Community Dental Care", message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.alert)
                                    let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (action) -> Void in
                                        showUnableToFindAlert()
                                        //                                        ///unable to find
                                        //                                        self.gotoPatientSignInForm()
                                    }
                                    alertController.addAction(alertYesAction)
                                    let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive) { (action) -> Void in
                                        
                                    }
                                    alertController.addAction(alertNoAction)
                                    self.present(alertController, animated: true, completion: nil)
                                }
                            }
                        }
                    }, failure: { (task, error) in
                        self.buttonNext.isUserInteractionEnabled = true
                        self.activityIndicator.stopAnimating()
                        self.patient.patientDetails = nil
                        if self.navigationController?.topViewController == self {
                            let alert = Extention.alert(error.localizedDescription)
                            self.present(alert, animated: true, completion: nil)
                        }
                })
            }
            
            if self.isNewPatient == true {
                gotoNextForm(true)
            } else {
                APICall()
            }
//            if self.isNewPatient == true {
//                self.gotoNextForm(true)
//            } else {
//                APICall()
//            }
        }
    }
    
//    func APICall() {
//        self.activityIndicator.startAnimating()
//        self.buttonNext.userInteractionEnabled = false
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMM dd, yyyy"
//        let date = dateFormatter.dateFromString(patient.dateOfBirth)
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        let manager = AFHTTPSessionManager(baseURL: NSURL(string: hostUrl))
//        manager.responseSerializer.acceptableContentTypes = ["text/html"]
//        manager.POST("consent_fetch_patient_info.php", parameters: ["first_name" : self.textFieldFirstName.text!, "last_name": self.textFieldLastName.text!, "dob": dateFormatter.stringFromDate(date!)], progress: { (progress) in
//            }, success: { (task, result) in
//                self.buttonNext.userInteractionEnabled = true
//                self.activityIndicator.stopAnimating()
//                let response = result as! [String : AnyObject]
//                
//                if response["status"] as! String == "success"  {
//                    let patientDetails = response["patientData"] as! [String: AnyObject]
//                    self.patient.patientDetails = PatientDetails(details: patientDetails)
//                    self.gotoNextForm(true)
//                } else {
//                    if response["status"] as! String == "matching_name"  {
//                        let patientDetails = response["matchingData"] as! [String: String]
//                        let alertController = UIAlertController(title: "Community Dental Care", message: "Did you mean \(patientDetails["Fname"]!) \(patientDetails["Lname"]!)? If so select YES and choose the correct date of birth", preferredStyle: UIAlertControllerStyle.Alert)
//                        let alertOkAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
//                            self.textFieldFirstName.text = patientDetails["Fname"]
//                            self.textFieldLastName.text = patientDetails["Lname"]
//                            self.textfieldDate.text = ""
//                            self.textfieldYear.text = ""
//                            self.textfieldMonth.text = ""
//                        }
//                        alertController.addAction(alertOkAction)
//                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                            self.patient.patientDetails = nil
//                            self.gotoNextForm(true)
//                        }
//                        alertController.addAction(alertNoAction)
//                        self.presentViewController(alertController, animated: true, completion: nil)
//                    } else {
//                        self.patient.patientDetails = nil
//                        let alertController = UIAlertController(title: "Community Dental Care", message: "Patient not found. Are you sure the provided details are correct?", preferredStyle: UIAlertControllerStyle.Alert)
//                        let alertYesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.Default) { (action) -> Void in
//                            self.gotoNextForm(true)
//                        }
//                        alertController.addAction(alertYesAction)
//                        let alertNoAction = UIAlertAction(title: "No", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                            
//                        }
//                        alertController.addAction(alertNoAction)
//                        self.presentViewController(alertController, animated: true, completion: nil)
//                    }
//                }
//            }, failure: { (task, error) in
//                self.buttonNext.userInteractionEnabled = true
//                self.activityIndicator.stopAnimating()
//                self.patient.patientDetails = nil
//                let alert = Extention.alert(error.localizedDescription)
//                self.presentViewController(alert, animated: true, completion: nil)
//                //                        gotoNextForm()
//        })
//    }
    
    
    
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
    
    
    
}

extension PatientInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == textfieldMiddleInitial {
//            return textField.formatInitial(range, string: string)
//        }else 
        if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        } else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

