//
//  HomeViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 17/02/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


public let screenSize = UIScreen.main.bounds



class HomeViewController: PDViewController {
    
    
    @IBOutlet weak var textFieldToothNumbers: PDTextField!
    @IBOutlet weak var labelAlertHeader: UILabel!
    @IBOutlet weak var labelAlertFooter: UILabel!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet var viewToothNumbers: PDView!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var viewAlert: PDView!
    @IBOutlet weak var buttonLogout: UIButton!
    
    @IBOutlet weak var labelVersion: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    var consentIndex : Int = 8
    
    var isNewPatient: Bool! = false
    
    @IBOutlet weak var constraintTableViewTop: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    
    
    var medicalHistoryOfficeComments : String = ""
    var medicalHistoryOfficeSign : UIImage = UIImage()
    
    var dentalHistoryOfficeComments : String = ""
    var dentalHistoryOfficeSign : UIImage = UIImage()
    
    var childMedicalHistoryOfficeComments : String = ""
    var childMedicalHistoryOfficeSign : UIImage = UIImage()
    
    var childDentalHistoryOfficeComments : String = ""
    var childDentalHistoryOfficeSign : UIImage = UIImage()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showAlert as (HomeViewController) -> () -> ()), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        // Do any additional setup after loading the view, typically from a nib.
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        labelDate.text = dateFormatter.string(from: Date()).uppercased()
    }
    
    
    @IBAction func buttonActionHistory() {
        let loginVC = mainStoryboard.instantiateViewController(withIdentifier: "kLoginViewController") as! LoginViewController
        loginVC.isStaffLogin = true
        let navVC = UINavigationController(rootViewController: loginVC)
        navVC.setNavigationBarHidden(true, animated: true)
        self.present(navVC, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: "Community Dental Care", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        
    }
    
    //    @IBAction func buttonActionNext(sender: AnyObject) {
    //        selectedForms.removeAll()
    //        for (_, form) in formList.enumerate() {
    //            if form.isSelected == true {
    //                if form.formTitle == kConsentForms  {
    //                    for subForm in form.subForms {
    //                        if subForm.isSelected == true {
    //                            selectedForms.append(subForm)
    //                        }
    //                    }
    //                } else {
    //                    selectedForms.append(form)
    //                }
    //            }
    //        }
    //        self.view.endEditing(true)
    //        if selectedForms.count > 0 {
    //            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
    //                return formObj1.index < formObj2.index
    //            })
    //        }
    //
    //        let patient = PDPatient(forms: selectedForms)
    //        patient.dateToday = labelDate.text
    //        patient.medicalHistory.medicalHistoryOfficeComments = self.medicalHistoryOfficeComments
    //        patient.medicalHistory.medicalHistoryOfficeSign = self.medicalHistoryOfficeSign
    //
    //        patient.childMedicalHistory.childMedicalHistoryOfficeComments = self.childMedicalHistoryOfficeComments
    //        patient.childMedicalHistory.childMedicalHistoryOfficeSign = self.childMedicalHistoryOfficeSign
    //
    //        patient.dentalHistory.dentalHistoryOfficeComments = self.dentalHistoryOfficeComments
    //        patient.dentalHistory.dentalHistoryOfficeSign = self.dentalHistoryOfficeSign
    //
    //        patient.childDentalHistory.childDentalHistoryOfficeComments = self.childDentalHistoryOfficeComments
    //        patient.childDentalHistory.childDentalHistoryOfficeSign = self.childDentalHistoryOfficeSign
    //
    //
    //
    //        let formNames = (patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
    //
    //        if formNames.contains(kVisitorCheckInForm) && selectedForms.count > 0{
    //
    //            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kVisitorVC") as! VisitorCheckinVC
    //            patientInfoVC.patient = patient
    //            self.navigationController?.pushViewController(patientInfoVC, animated: true)
    //
    //        }else if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
    //
    //            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedBackInfoViewController") as! FeedBackInfoViewController
    //            patientInfoVC.patient = patient
    //            self.navigationController?.pushViewController(patientInfoVC, animated: true)
    //        }else if selectedForms.count > 0 {
    //
    //            let patientInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientInfoVC") as! PatientInfoViewController
    //            patientInfoVC.patient = patient
    //            self.navigationController?.pushViewController(patientInfoVC, animated: true)
    //        } else {
    //            let alert = Extention.alert("PLEASE SELECT ANY FORM")
    //            self.presentViewController(alert, animated: true, completion: nil)
    //        }
    //    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        selectedForms.removeAll()
        for form in formList {
            if form.isSelected == true {
                for subForm in form.subForms {
                    if subForm.isSelected == true {
                        selectedForms.append(subForm)
                    }
                }
            }
        }
        self.view.endEditing(true)
        if selectedForms.count > 0 {
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 122
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                self.constraintTableViewBottom.constant = 218
            }) 
            let formNames = (selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            let patient = PDPatient(forms: selectedForms)
            patient.dateToday = labelDate.text
            patient.medicalHistory.medicalHistoryOfficeComments = self.medicalHistoryOfficeComments
            patient.medicalHistory.medicalHistoryOfficeSign = self.medicalHistoryOfficeSign
            
            patient.childMedicalHistory.childMedicalHistoryOfficeComments = self.childMedicalHistoryOfficeComments
            patient.childMedicalHistory.childMedicalHistoryOfficeSign = self.childMedicalHistoryOfficeSign
            
            patient.dentalHistory.dentalHistoryOfficeComments = self.dentalHistoryOfficeComments
            patient.dentalHistory.dentalHistoryOfficeSign = self.dentalHistoryOfficeSign
            
            patient.childDentalHistory.childDentalHistoryOfficeComments = self.childDentalHistoryOfficeComments
            patient.childDentalHistory.childDentalHistoryOfficeSign = self.childDentalHistoryOfficeSign

            if formNames.contains(kVisitorCheckInForm) && selectedForms.count > 0{
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kVisitorVC") as! VisitorCheckinVC
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }else if selectedForms.count == 1 && selectedForms[0].formTitle == kFeedBack {
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }
            else {
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
                patientInfoVC.patient = patient
                patientInfoVC.isNewPatient = self.isNewPatient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }
        } else {
            let alert = Extention.alert("PLEASE SELECT ANY FORM")
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func buttonActionLogout(_ sender: AnyObject) {
        //        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kKeychainItemName)
        //        buttonLogout.hidden = true
    }
    
    
    
    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
        textFieldToothNumbers.resignFirstResponder()
        self.viewToothNumbers.removeFromSuperview()
        self.viewShadow.isHidden = true
        let form =  textFieldToothNumbers.tag <= consentIndex ? formList[textFieldToothNumbers.tag] : formList[consentIndex].subForms[textFieldToothNumbers.tag - (consentIndex + 1)]
        if !textFieldToothNumbers.isEmpty || textFieldToothNumbers.tag == 12 {
            form.isSelected = true
            form.toothNumbers = textFieldToothNumbers.text
        } else {
            form.isSelected = false
        }
        self.tableViewForms.reloadData()
        
    }
    
    func showAlert() {
        viewAlert.isHidden = false
        viewShadow.isHidden = false
    }
    
    @IBAction func buttonActionOk(_ sender: AnyObject) {
        
        viewAlert.isHidden = true
        viewShadow.isHidden = true
    }
    
    
    func showPopup() {
        textFieldToothNumbers.text = ""
        self.viewToothNumbers.frame = CGRect(x: 0, y: 0, width: 512.0, height: 250.0)
        self.viewToothNumbers.center = self.view.center
        self.viewShadow.addSubview(self.viewToothNumbers)
        self.viewToothNumbers.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewToothNumbers.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    
    
}

extension HomeViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location)..<textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = textField.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                textField.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    textField.text = "0" + textFieldString
                    return false
                }
                
            }
            if textFieldString == "," {
                return false
            }
        }
        
        
        
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}


extension HomeViewController : UITableViewDelegate {
    
    func tapGestureDidSelectAction(_ sender : UITapGestureRecognizer) {
        let indexPath = (sender.view as! FormsTableViewCell).indexPath!
        let form = formList[indexPath.section].subForms[indexPath.row]
        form.isSelected = !form.isSelected
        if indexPath.section == 0 {
            if formList[0].subForms[0].isSelected == false {
                formList[0].subForms[1].isSelected = false
                formList[0].subForms[2].isSelected = false
                formList[0].subForms[3].isSelected = false
                formList[0].subForms[4].isSelected = false
                formList[0].subForms[5].isSelected = false
                formList[0].subForms[6].isSelected = false

            }
            tableViewForms.reloadSections(IndexSet(integer: 0), with: UITableViewRowAnimation.none)
            return
        }

        tableViewForms.reloadRows(at: [indexPath], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 102.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let form = formList[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! FormsTableViewCell
        let headerView = cell.contentView.subviews[1] as! PDTableHeaderView
        headerView.labelFormName.text = form.formTitle
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            if selectedForm.count > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.contentView.alpha = form.isSelected == false ? 0.2 : 1.0
                }) 
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.contentView.alpha = 1.0
                }) 
            }
        }
        
        
        headerView.tag = section
        if headerView.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            headerView.addGestureRecognizer(tapGesture)
        }
        return cell.contentView
    }
    
    func tapGestureAction(_ sender : UITapGestureRecognizer) {
        let headerView = sender.view as! PDTableHeaderView
        let form = self.formList[headerView.tag]
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        
        func displayRows() {
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 30
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                self.constraintTableViewBottom.constant = 30
                self.isNewPatient = form.formTitle == kNewPatient.keys.first ? true : false
                form.isSelected = !form.isSelected
                var indexPaths : [IndexPath] = [IndexPath]()
                for (idx, _) in form.subForms.enumerated() {
                    let indexPath = IndexPath(row: idx, section: headerView.tag)
                    indexPaths.append(indexPath)
                }
                self.tableViewForms.beginUpdates()
                self.tableViewForms.insertRows(at: indexPaths, with: .fade)
                self.tableViewForms.endUpdates()
                let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.tableViewForms.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        self.tableViewForms.isUserInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    }
                }
            }) 
        }
        
        self.tableViewForms.isUserInteractionEnabled = false
        if selectedForm.count > 0 {
            selectedForm[0].isSelected = false
            let section = selectedForm[0].formTitle == kNewPatient.keys.first ? 0 : selectedForm[0].formTitle == kExistingPatient.keys.first ? 1 : 2
            var indexPaths : [IndexPath] = [IndexPath]()
            
            for (idx, _) in selectedForm[0].subForms.enumerated() {
                let indexPath = IndexPath(row: idx, section: section)
                indexPaths.append(indexPath)
            }
            if form.formTitle == kNewPatient.keys.first {
                for obj in self.formList {
                    for subForm in obj.subForms {
                        subForm.isSelected = false
                    }
                }
            }
            self.tableViewForms.beginUpdates()
            self.tableViewForms.deleteRows(at: indexPaths, with: .none)
            self.tableViewForms.endUpdates()
            
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 122
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                self.constraintTableViewBottom.constant = 218
                if form.formTitle != selectedForm[0].formTitle {
                    let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        displayRows()
                    }
                } else {
                    let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        self.tableViewForms.isUserInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    }
                }
            }) 
        } else {
            displayRows()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm") as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        let height = form.formTitle.heightWithConstrainedWidth(350.0, font: cell.labelFormName.font) + 20
        return height
    }
    
}

extension HomeViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return formList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let form = formList[section]
        return form.isSelected == true ? form.subForms.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.isHidden = !form.isSelected
        cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white
        cell.indexPath = indexPath
        if indexPath.section == 0 && indexPath.row > 0 {
            cell.labelFormName.textColor = formList[0].subForms[0].isSelected == true ? (form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white) : UIColor.lightGray
        } else {
            cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white
        }

        if cell.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureDidSelectAction))
            cell.addGestureRecognizer(tapGesture)
        }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        
        return cell
    }
}


