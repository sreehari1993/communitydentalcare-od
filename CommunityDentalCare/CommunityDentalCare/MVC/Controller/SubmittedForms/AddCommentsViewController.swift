//
//  AddCommentsViewController.swift
//  WestKendall
//
//  Created by Berlin Raj on 10/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol AddCommentsViewControllerDelegate {
    func submittedDetails(patient: PDPatient)
}

class AddCommentsViewController: PDViewController {
    
    @IBOutlet weak var textViewComments: PDTextView!
    @IBOutlet weak var signatureView: SignatureView!
    
    @IBOutlet weak var dateLabel: DateLabel!
    
    var delegate: AddCommentsViewControllerDelegate?
    
    var pendingForm: PendingForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kCommonDateFormat
        
        dateLabel.todayDate = dateFormatter.string(from: Date())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   @IBAction override func buttonActionBack(withSender sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
   @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
        
        if !signatureView.isSigned()  {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !dateLabel.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
        self.view.endEditing(false)
        
        let form = Forms()
        form.formTitle = self.pendingForm.formName
        let patient = PDPatient(forms: [form])
        
        patient.firstName = pendingForm.firstName
        patient.lastName = pendingForm.lastName
        patient.dateToday = dateLabel.text!
        patient.medCommentsValue = textViewComments.text
        patient.dentistInitials = signatureView.signatureImage()
        patient.pendingForm = self.pendingForm
        
        //NewValues
        
        self.dismiss(animated: true, completion: {
            self.delegate?.submittedDetails(patient: patient)
        })
        }
    }

}
