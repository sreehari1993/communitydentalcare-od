//
//  PendingFormDetailVC.swift
//  WestKendall
//
//  Created by Berlin Raj on 10/27/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol PendingFormDetailViewControllerDelegate {
    func formSubmitted(form: PendingForm)
}

class PendingFormDetailViewController: PDViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var labelComments1: [UILabel]!

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var dateLabel: UILabel!

    
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTopSpace: NSLayoutConstraint!
    
    var delegate: PendingFormDetailViewControllerDelegate?
    
    var pendingForm: PendingForm!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = pendingForm.formImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
        let addCommentsVC = mainStoryboard.instantiateViewController(withIdentifier: "kAddCommentsViewController") as! AddCommentsViewController
        addCommentsVC.delegate = self
        addCommentsVC.pendingForm = pendingForm
        self.present(addCommentsVC, animated: true, completion: nil)
    }
    
  override func sendFormToGoogleDrive(_ completion: @escaping (_ success: Bool) -> Void) {
        self.completion = completion
        super.buttonActionSubmit(withSender: buttonSubmit!)
    }
    
  
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        constraintViewHeight.constant =  pendingForm.formName == kDentalHistory.fileName ? 2048 : 1024
        
       constraintTopSpace.constant = pendingForm.formName == kDentalHistory.fileName ? 1024.0 + 203.0 : 1024
        
        
    }
}

extension PendingFormDetailViewController: AddCommentsViewControllerDelegate {
    
    func submittedDetails(patient: PDPatient) {
        //Display Values
        
       patient.medCommentsValue.setTextForArrayOfLabels(labelComments1)
        //signatureView.isHidden = true
       signatureView.image = patient.dentistInitials
        dateLabel.text = patient.dateToday
        self.patient = patient
        self.sendFormToGoogleDrive({ (success) in
            if success {
                BRProgressHUD.show()
                self.pendingForm.submitCompletedForm(completion: { (success, error) in
                    BRProgressHUD.hide()
                    if success {
                        self.delegate?.formSubmitted(form: self.pendingForm)
                        _ = self.navigationController?.popViewController(animated: true)
                    } else {
                        self.showAlert(error!.localizedDescription.uppercased())
                    }
                })
            } else {
                BRProgressHUD.hide()
                self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            }
        })
    }
}
