//
//  InformedConsentFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedConsentFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet var buttonTechniques : [RadioButton]!
    @IBOutlet weak var signatureParent : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        signatureParent.image = patient.informedSignatureParent
        signatureWitness.image = patient.informedSignatureWitness
        signatureDoctor.image = patient.informedSignatureDoctor
        for btn in buttonTechniques{
            btn.isSelected = patient.informedConsentTechniqueTag.contains(btn.tag)
        }
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
