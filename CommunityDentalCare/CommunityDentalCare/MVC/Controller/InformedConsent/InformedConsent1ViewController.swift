//
//  InformedConsent1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class InformedConsent1ViewController: PDViewController {
    
    var arrayTechniquesTag : [Int] = [Int]()
    @IBOutlet weak var signatureParent : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var signatureDoctor : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var labelDate3 : DateLabel!
    @IBOutlet var arrayButtons : [UIButton]!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func saveValues (){
        patient.informedConsentTechniqueTag = arrayTechniquesTag

    }
    
    func loadValues()  {
        arrayTechniquesTag = patient.informedConsentTechniqueTag
        for btn in arrayButtons{
            btn.isSelected = patient.informedConsentTechniqueTag.contains(btn.tag)
        }
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        ///self.navigationController?.popViewControllerAnimated(true)
    }

    @IBAction func ontechniquesButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if arrayTechniquesTag.contains(sender.tag){
            arrayTechniquesTag.remove(at: arrayTechniquesTag.index(of: sender.tag)!)
            arrayButtons[sender.tag - 1].isSelected = false
        }else{
            arrayTechniquesTag.append(sender.tag)
            arrayButtons[sender.tag - 1].isSelected = true

        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signatureDoctor.isSigned() || !signatureParent.isSigned() || !signatureWitness.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)

        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)

        }else{
            saveValues()
            patient.informedSignatureDoctor = signatureDoctor.signatureImage()
            patient.informedSignatureParent = signatureParent.signatureImage()
            patient.informedSignatureWitness = signatureWitness.signatureImage()
            let medical = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildMedicalFormVC") as! ChildMedicalHistoryFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)

//            let informed = mainStoryboard.instantiateViewControllerWithIdentifier("InformedConsentFormVC") as! InformedConsentFormViewController
//            informed.patient = self.patient
//            self.navigationController?.pushViewController(informed, animated: true)
            
        }
        
    }


}
