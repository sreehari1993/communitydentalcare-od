//
//  CardImageCaptureVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageCaptureVC: PDViewController {

    
    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
//    @IBOutlet weak var backButton: PDButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    var isDrivingLicense: Bool = false
    var isFrontImageSelected: Bool = false
    var isBackImageSelected: Bool = false
    
    var frontPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseFront") : UIImage(named: "InsuranceFront")
        }
    }
    
    var backPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseBack") : UIImage(named: "InsuranceBack")
        }
    }
//    @IBOutlet weak var captureButtonFront: UIButton!
//    @IBOutlet weak var captureButtonBack: UIButton!

    var selectedButton: UIButton!
    
//    var overlayView: UIView {
//        get {
//            let imageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
//            imageView.backgroundColor = UIColor.clearColor()
//            imageView.image = UIImage(named: "overlay")
//            return imageView
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelTitle.text = isDrivingLicense ? "ADD/UPDATE DRIVING LICENSE" : "ADD/UPDATE INSURANCE CARD"
        
        imageViewFront.cornerRadius = 5.0
        imageViewBack.cornerRadius = 5.0
        imageViewBack.borderColor = UIColor.clear
        imageViewFront.borderColor = UIColor.clear
        
        imageViewFront.image = self.frontPlaceHolderImage
        imageViewBack.image = self.backPlaceHolderImage
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func nextAction(_ sender: AnyObject) {
        if isDrivingLicense && !isFrontImageSelected {
            let alert = Extention.alert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            self.present(alert, animated: true, completion: nil)
        } else if !isDrivingLicense && !isFrontImageSelected  {
            let alert = Extention.alert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
            self.present(alert, animated: true, completion: nil)
        } else {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kCardImageFormVC") as! CardImageFormVC
            formVC.patient = self.patient
            formVC.isDrivingLicense = self.isDrivingLicense
            formVC.frontImage = self.imageViewFront.image!
            formVC.backImage = isBackImageSelected ? self.imageViewBack.image : nil
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    @IBAction func camButtonSelected(_ sender: UIButton) {
//        let picker = self.storyboard?.instantiateViewControllerWithIdentifier("kWSImagePickerController") as! WSImagePickerController
//        picker.delegate = self
//        self.presentViewController(picker, animated: true) { 
//            self.selectedButton = sender
        let picker = self.storyboard?.instantiateViewController(withIdentifier: "kCardImagePickerControllerNew") as! CardImagePickerControllerNew
        picker.delegate = self
        self.present(picker, animated: true) {
            self.selectedButton = sender
        }

        }
//        let pickerView = UIImagePickerController()
//        pickerView.allowsEditing = false
//        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
//            pickerView.sourceType = UIImagePickerControllerSourceType.Camera
//            if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.Front) {
//                pickerView.cameraDevice = UIImagePickerControllerCameraDevice.Front
//                if UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front) {
//                    pickerView.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                }
//            } else {
//                pickerView.cameraDevice = UIImagePickerControllerCameraDevice.Rear
//                if UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Rear) {
//                    pickerView.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                }
//            }
//            pickerView.cameraOverlayView = self.overlayView
//        } else {
//            pickerView.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
//        }
//        pickerView.mediaTypes = [kUTTypeImage as String]
//        pickerView.delegate = self
//        
//        self.presentViewController(pickerView, animated: true, completion: {
//            self.selectedButton = sender
//        })
    }

extension CardImageCaptureVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate, CardImageCaptureDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
    class func getCardImage(OfImage originalImage: UIImage?) -> UIImage? {
        if originalImage == nil {
            return nil
        } else {
            let deviceScale = UIScreen.main.scale
            
            UIGraphicsBeginImageContext(CGSize(width: deviceScale * 502, height: deviceScale * 326.5))
            originalImage!.draw(in: CGRect(x: -133 * deviceScale, y: -294 * deviceScale, width: deviceScale * 768, height: deviceScale * 1024))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = CardImageCaptureVC.getCardImage(OfImage: info[UIImagePickerControllerOriginalImage] as? UIImage) {
            if selectedButton.tag == 1 {
                imageViewFront.image = image
                isFrontImageSelected = true
                selectedButton.isSelected = true
                
                imageViewFront.borderColor = UIColor.white
            } else {
                imageViewBack.image = image
                isBackImageSelected = true
                selectedButton.isSelected = true
                
                imageViewBack.borderColor = UIColor.white
            }
        }
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
    
    func cardImagePicker(_ picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?) {
        if image != nil {
            if selectedButton.tag == 1 {
                imageViewFront.image = image
                isFrontImageSelected = true
                selectedButton.isSelected = true
                
                imageViewFront.borderColor = UIColor.white
            } else {
                imageViewBack.image = image
                isBackImageSelected = true
                selectedButton.isSelected = true
                
                imageViewBack.borderColor = UIColor.white
            }
        }
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
    
    func cardImagePickerDidCancel(_ picker: CardImagePickerControllerNew) {
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
}

