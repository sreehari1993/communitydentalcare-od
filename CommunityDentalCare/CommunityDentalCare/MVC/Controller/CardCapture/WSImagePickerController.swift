//
//  WSImagePickerController.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class WSImagePickerController: UIImagePickerController {

    @IBOutlet weak var overlayView: UIView?
    var capturePressed: Bool = false
//        {
//        didSet {
    
//        }
//    }
    
//    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var camDeviceButton: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.allowsEditing = false
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            self.sourceType = UIImagePickerControllerSourceType.camera
            if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.rear) {
                self.cameraDevice = UIImagePickerControllerCameraDevice.rear
                if UIImagePickerController.isFlashAvailable(for: UIImagePickerControllerCameraDevice.rear) {
                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.on
                } else {
                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.off
                }
            } else {
                self.cameraDevice = UIImagePickerControllerCameraDevice.front
                if UIImagePickerController.isFlashAvailable(for: UIImagePickerControllerCameraDevice.front) {
                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.on
                }
            }
            self.showsCameraControls = false
            self.cameraOverlayView = self.overlayView
        } else {
            self.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        self.mediaTypes = [kUTTypeImage as String]
        
//        if !UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Rear) && !UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front) {
//            self.flashButton.enabled = false
//        }
        // Do any additional setup after loading the view.
    }
//    override func viewWillAppear(animated: Bool) {
//        super.viewWillAppear(animated)
//        self.cameraOverlayView = self.overlayView
//    }
//    @IBAction func flashButtonAction(sender: UIButton) {
//        if sender.selected {
//            //Flash OFF
//            self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
//            sender.selected = false
//        } else {
//            //Flash ON
//            if camDeviceButton.selected && UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Rear) {
//                self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                sender.selected = true
//            } else if !camDeviceButton.selected && UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front) {
//                self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                sender.selected = true
//            } else {
//                self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
//                sender.selected = false
//            }
//        }
//    }
    
    @IBAction func camShotPressed(_ sender: UIButton) {
        capturePressed = true
        self.takePicture()
//        self.view.userInteractionEnabled = true
    }
    
    @IBAction func cameraDeviceButtonAction(_ sender: UIButton) {
        if capturePressed == true {
            capturePressed = false
            return
        }
        if !sender.isSelected {
            if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.front) {
                self.cameraDevice = UIImagePickerControllerCameraDevice.front
//                if flashButton.selected && UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front) {
//                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                } else {
//                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
//                    self.flashButton.selected = false
//                }
                sender.isSelected = true
            }
        } else {
            if UIImagePickerController.isCameraDeviceAvailable(UIImagePickerControllerCameraDevice.rear) {
                self.cameraDevice = UIImagePickerControllerCameraDevice.rear
//                if flashButton.selected && UIImagePickerController.isFlashAvailableForCameraDevice(UIImagePickerControllerCameraDevice.Front) {
//                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.On
//                } else {
//                    self.cameraFlashMode = UIImagePickerControllerCameraFlashMode.Off
//                    self.flashButton.selected = false
//                }
                sender.isSelected = false
            }
        }
    }
    @IBAction func backAction(_ sender: UIButton) {
        if capturePressed == true {
            capturePressed = false
            return
        }
        self.delegate?.imagePickerControllerDidCancel!(self)
    }
    func getCardImage(_ originalImage: UIImage?) -> UIImage? {
        
        if originalImage == nil {
            return nil
        } else {
            let deviceScale = UIScreen.main.scale
//            UIGraphicsBeginImageContext(CGSize(width: deviceScale * 270, height: deviceScale * 175.5))
//            
//            originalImage!.drawInRect(CGRectMake(-249 * deviceScale, -369.5 * deviceScale, deviceScale * 768, deviceScale * 1024))
            
            UIGraphicsBeginImageContext(CGSize(width: deviceScale * 502, height: deviceScale * 326.5))
            originalImage!.draw(in: CGRect(x: -133 * deviceScale, y: -294 * deviceScale, width: deviceScale * 768, height: deviceScale * 1024))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return image
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .portrait
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
