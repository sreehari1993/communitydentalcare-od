//
//  MedicalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory2ViewController: PDViewController {
    
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var labelHeading : UILabel!
    var selectedIndex: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.stopAnimating()
        if let medicalQuestions = patient.patientDetails?.medicalQuestions {
            for question in self.patient.medicalHistory.medicalQuestions[0] {
                question.selectedOption = medicalQuestions.contains(question.question)
            }
        }
        if let problems = patient.patientDetails?.problems {
            for question in self.patient.medicalHistory.medicalQuestions[1] {
                question.selectedOption = problems.contains(question.question)
            }
            for question in self.patient.medicalHistory.medicalQuestions[2] {
                question.selectedOption = problems.contains(question.question)
            }
        }
        self.patient.medicalHistory.medicationsTag.removeAll()
        let values = ["Blood Thinners", "Steroids", "Nitroglycerin", "Bisphosphonates"]
        if let medications = patient.patientDetails?.medications {
            for medication in medications {
                if let index = values.index(of: medication) {
                    self.patient.medicalHistory.medicationsTag.append(index + 1)
                } else {
                    if !self.patient.medicalHistory.medicationsTag.contains(5) {
                        self.patient.medicalHistory.medicationsTag.append(5)
                        self.patient.medicalHistory.medicationOthers = medication
                    }
                }
            }
            
            if medications.count > 0 {
                self.patient.medicalHistory.medicalQuestions[0][3].selectedOption = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                self.tableViewQuestions.reloadData()
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == 2 {
                let medical = medicalStoryboard.instantiateViewController(withIdentifier: "Medical3VC") as! MedicalHistory3ViewController
                medical.patient = self.patient
                self.navigationController?.pushViewController(medical, animated: true)
                
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.labelHeading.text = self.selectedIndex == 0 ? "" : "Have you ever had any of the following: *"
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    
}

extension MedicalHistory2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.medicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        
        return cell
    }
}

extension MedicalHistory2ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
        if selectedIndex == 0 {
            if cell.tag == 2{
                PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                    if isEdited{
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                    }else{
                        cell.radioButtonYes.isSelected = false
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                    }
                })
            }else if cell.tag == 3{
                MedicationAlert.sharedInstance.showPopUp(self.view, patient: self.patient, completion: { (arrayTags, other) in
                    if arrayTags.count == 0{
                        cell.radioButtonYes.isSelected = false
                        self.patient.medicalHistory.medicationsTag.removeAll()
                        self.patient.medicalHistory.medicationOthers = "N/A"
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false

                        
                    }else{
                        self.patient.medicalHistory.medicationsTag = arrayTags
                        self.patient.medicalHistory.medicationOthers = other
                        
                    }
                    
                    }, showAlert: { (alertMessage) in
                        let alert = Extention.alert(alertMessage)
                        self.present(alert, animated: true, completion: nil)
                })
            }else if cell.tag == 9{
                YesOrNoAlert.sharedInstance.showWithTitle("Do you have high or low blood pressure?", button1Title: "HIGH", button2Title: "LOW", completion: { (buttonIndex) in
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = buttonIndex == 1 ? "HIGH" : "LOW"
                    
                })
                
            }
        }else if selectedIndex == 2{
            if cell.tag == 11{
                PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
                    if isEdited{
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                    }else{
                        cell.radioButtonYes.isSelected = false
                        self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false

                    }
                })
            }
            
        }
        
    }
}

