//
//  MedicalHistory1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory1ViewController: PDViewController {
    
    @IBOutlet weak var textfieldPhysicianName : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldPhone : UITextField!
    @IBOutlet weak var textfieldDentalExam : UITextField!
    @IBOutlet weak var textfieldLowBp : UITextField!
    @IBOutlet weak var textfieldHighBp : UITextField!
    @IBOutlet weak var textfieldPulse : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addDatePickerForTextField(textfieldDentalExam)
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        super.loadView()
        BRProgressHUD.show()
        ServiceManager.fetchDataFromService(hostUrl, serviceName: "consent_fetch_medical_questions.php", parameters: ["patient_id": patient.patientDetails!.patientNumber], success: { (result) in
            BRProgressHUD.hide()
            //print(result)
            if let questions = result["patientQuestions"] as? [String], questions.count > 0 {
                self.patient.patientDetails?.medicalQuestions = [String]()
                self.patient.patientDetails?.medicalQuestions = questions
            }
            if let medications = result["medications"] as? [String], medications.count > 0 {
                self.patient.patientDetails?.medications = [String]()
                self.patient.patientDetails?.medications = medications
            }
            if let problems = result["problems"] as? [String], problems.count > 0 {
                self.patient.patientDetails?.problems = [String]()
                self.patient.patientDetails?.problems = problems
            }
            }) { (error) in
                BRProgressHUD.hide()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.medicalHistory.physicianName = textfieldPhysicianName.isEmpty ? "N/A" : textfieldPhysicianName.text!
        patient.medicalHistory.physicianAddress = textfieldAddress.isEmpty ? "N/A" : textfieldAddress.text!
        patient.medicalHistory.lastDentalExam = textfieldDentalExam.isEmpty ? "N/A" : textfieldDentalExam.text!
        patient.medicalHistory.PhysicianPhone = textfieldPhone.isEmpty ? "N/A" : textfieldPhone.text!
        patient.medicalHistory.bloodpressureLow = textfieldLowBp.isEmpty ? "" : textfieldLowBp.text!
        patient.medicalHistory.bloodpressureHigh = textfieldHighBp.isEmpty ? "" : textfieldHighBp.text!
        patient.medicalHistory.pulse = textfieldPulse.isEmpty ? "N/A" : textfieldPulse.text!
    }
    
    func loadValues()  {
        textfieldPhysicianName.setSavedText(patient.medicalHistory.physicianName)
        textfieldAddress.setSavedText(patient.medicalHistory.physicianAddress)
        textfieldDentalExam.setSavedText(patient.medicalHistory.lastDentalExam)
        textfieldPhone.setSavedText(patient.medicalHistory.PhysicianPhone)
        textfieldLowBp.setSavedText(patient.medicalHistory.bloodpressureLow)
        textfieldHighBp.setSavedText(patient.medicalHistory.bloodpressureHigh)
        textfieldPulse.setSavedText(patient.medicalHistory.pulse)
    }

    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
         if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
        }else{
            saveValues()
            let medical = medicalStoryboard.instantiateViewController(withIdentifier: "Medical2VC") as! MedicalHistory2ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }
    }
}

extension MedicalHistory1ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldLowBp || textField == textfieldPulse || textField == textfieldHighBp{
            return textField.formatNumbers(range, string: string, count: 3)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
