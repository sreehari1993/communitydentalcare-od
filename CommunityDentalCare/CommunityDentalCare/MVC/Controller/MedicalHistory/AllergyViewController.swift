//
//  AllergyViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 10/6/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AllergyViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: UIButton!
    @IBOutlet var buttonBack1: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()
        fetchAllergies()
    }
    
    var allergyArray: [Allergy] {
        get {
            if let patientDetails = patient.patientDetails {
                if patientDetails.allergySelected.count > 0 {
                    return patientDetails.allergySelected
                } else {
                    return patient.allergyArray
                }
            } else {
                return patient.allergyArray
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchAllergies()  {
        BRProgressHUD.show()
        if let _ = patient.patientDetails {
            BRProgressHUD.hide()
            self.tableViewQuestions.reloadData()
        } else {
            Allergy.getAllergyList({ (result) in
                BRProgressHUD.hide()
                self.patient.allergyArray = result
                self.tableViewQuestions.reloadData()
            }) { (error) in
                BRProgressHUD.hide()
                self.patient.allergyArray.removeAll()
                self.tableViewQuestions.reloadData()
                self.showAlert(error!.localizedDescription)
            }
        }
    }
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack1.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack1.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            if selectedIndex == max(0, allergyArray.count/15)  {
                let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
                if formNames.contains(kMedicalHistoryForm){
                    let medical = medicalStoryboard.instantiateViewController(withIdentifier: "Medical4VC") as! MedicalHistory4ViewController
                    medical.patient = self.patient
                    self.navigationController?.pushViewController(medical, animated: true)
                }else{
                    let medical = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildMedical2VC") as! ChildMedicalHistory2ViewController
                    medical.patient = self.patient
                    self.navigationController?.pushViewController(medical, animated: true)

                }
                
            } else {
                buttonBack1.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.tableViewQuestions.reloadData()
                    self.buttonBack1.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    
}

extension AllergyViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allergyArray.count - (selectedIndex * 15) > 14 ? 15 : allergyArray.count - (selectedIndex * 15)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AllergyTableViewCell
        let indexValue = (selectedIndex * 15) + indexPath.row
      //  cell.delegate = self
        cell.tag = indexPath.row
        cell.configureCell(self.allergyArray[indexValue], patnt: self.patient)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexValue = (selectedIndex * 15) + indexPath.row

        let allgy : Allergy = self.allergyArray[indexValue]
        allgy.isSelected = !allgy.isSelected
        if allgy.isSelected == true {
            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE ENTER DESCRIPTION", completion: { (textView, isEdited) in
                if isEdited{
                    allgy.allergyDescription = textView.text
                }else{
                    allgy.allergyDescription = ""
                }
            })
            PopupTextView.sharedInstance.setString(allgy.allergyDescription == nil ? "" : allgy.allergyDescription)
        }
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)

    }
}
