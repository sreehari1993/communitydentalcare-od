//
//  MedicalHistory3ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory3ViewController: PDViewController {
    
    @IBOutlet weak var radioWoman : RadioButton!
    @IBOutlet weak var viewContainer : UIView!
    @IBOutlet weak var radioPregnant : RadioButton!
//    @IBOutlet var arrayAllergyButtons : [UIButton]!
    @IBOutlet var arrayTestButtons : [UIButton]!
    @IBOutlet var arraySmokeButtons : [UIButton]!
    @IBOutlet weak var radioAllergy : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
//        patient.medicalHistory.arrayAllergyTags = [Int]()
//        patient.medicalHistory.arrayTestTags = [Int]()
//        patient.medicalHistory.allergyOthers = "N/A"
        
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.medicalHistory.radioAllergyTag = radioAllergy.selected == nil ? 0 : radioAllergy.selected.tag
        patient.medicalHistory.radioPregnantTag = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.medicalHistory.radioWomanTag = radioWoman.selected.tag

    }
    
    func loadValues()  {
//        for btn in arrayAllergyButtons{
//            btn.selected = patient.medicalHistory.arrayAllergyTags.contains(btn.tag)
//        }
        for btn in arrayTestButtons{
            btn.isSelected = patient.medicalHistory.arrayTestTags.contains(btn.tag)
        }
        for btn in arraySmokeButtons{
            btn.isSelected = patient.medicalHistory.arraySmokeTag.contains(btn.tag)
        }
        radioAllergy.setSelectedWithTag(patient.medicalHistory.radioAllergyTag)
        radioWoman.setSelectedWithTag(patient.medicalHistory.radioWomanTag)
        radioPregnant.setSelectedWithTag(patient.medicalHistory.radioPregnantTag)
        if radioWoman.selected.tag == 1{
            viewContainer.isUserInteractionEnabled = true
            viewContainer.alpha = 1.0
        }else{
            viewContainer.isUserInteractionEnabled = false
            viewContainer.alpha = 0.5
            radioPregnant.deselectAllButtons()
            self.patient.medicalHistory.radioBirthControlTag = 0
            self.patient.medicalHistory.radioPregnantTag = 0
            
        }
        
        if let patientDetails = patient.patientDetails {
            if patientDetails.allergySelected.count > 0 && patient.medicalHistory.radioAllergyTag == 0 {
                radioAllergy.isSelected = true
            }
        }
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
//    @IBAction func allergyButtonAction (withSender sender : UIButton){
//        sender.selected = !sender.selected
//        if patient.medicalHistory.arrayAllergyTags.contains(sender.tag){
//            patient.medicalHistory.arrayAllergyTags.removeAtIndex(patient.medicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
//        }else{
//            patient.medicalHistory.arrayAllergyTags.append(sender.tag)
//        }
//        
//        if sender.tag == 8 && sender.selected == true{
//            PopupTextView.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", completion: { (textView, isEdited) in
//                if isEdited{
//                    self.patient.medicalHistory.allergyOthers = textView.text!
//                    
//                }else{
//                    self.patient.medicalHistory.allergyOthers = "N/A"
//                    sender.selected = false
//                    self.patient.medicalHistory.arrayAllergyTags.removeAtIndex(self.patient.medicalHistory.arrayAllergyTags.indexOf(sender.tag)!)
//                }
//            })
//            
//        }else if sender.tag == 8 && sender.selected == false{
//            self.patient.medicalHistory.allergyOthers = "N/A"
//        }
//        
//    }
    
    @IBAction func testButtonAction (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.medicalHistory.arrayTestTags.contains(sender.tag){
            patient.medicalHistory.arrayTestTags.remove(at: patient.medicalHistory.arrayTestTags.index(of: sender.tag)!)
        }else{
            patient.medicalHistory.arrayTestTags.append(sender.tag)
        }

    }
    
    @IBAction func smokeButtonAction (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.medicalHistory.arraySmokeTag.contains(sender.tag){
            patient.medicalHistory.arraySmokeTag.remove(at: patient.medicalHistory.arraySmokeTag.index(of: sender.tag)!)
        }else{
            patient.medicalHistory.arraySmokeTag.append(sender.tag)
        }

    }
    
    @IBAction func radioWomanButtonAction (withSender sender : RadioButton){
        if sender.tag == 1{
            viewContainer.isUserInteractionEnabled = true
            viewContainer.alpha = 1.0
        }else{
            viewContainer.isUserInteractionEnabled = false
            viewContainer.alpha = 0.5
            radioPregnant.deselectAllButtons()
            self.patient.medicalHistory.radioBirthControlTag = 0
            self.patient.medicalHistory.radioPregnantTag = 0
        }
        
    }
    
    @IBAction func radioPregnantButtonAction (withSender sender : UIButton){
        if sender.tag == 2{
        YesOrNoAlert.sharedInstance.showWithTitle("Are you taking birth control?", button1Title: "YES", button2Title: "NO") { (buttonIndex) in
            self.patient.medicalHistory.radioBirthControlTag = buttonIndex
        }
        }else{
            self.patient.medicalHistory.radioBirthControlTag = 0

        }
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if (radioWoman.selected.tag == 1 && radioPregnant.selected == nil) || radioAllergy.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)

        }else{
            saveValues()
            if radioAllergy.selected.tag == 1{
                let allergy = medicalStoryboard.instantiateViewController(withIdentifier: "AllergyVC") as! AllergyViewController
                allergy.patient = self.patient
                self.navigationController?.pushViewController(allergy, animated: true)

            }else{
            let medical = medicalStoryboard.instantiateViewController(withIdentifier: "Medical4VC") as! MedicalHistory4ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
            }

        }
        
    }
}
