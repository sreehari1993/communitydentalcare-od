//
//  MedicalHistoryFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelBirthDate : UILabel!
    @IBOutlet weak var labelBp : UILabel!
    @IBOutlet weak var labelPulse : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianPhone : UILabel!
    @IBOutlet weak var labelPhysicianAddress : UILabel!
    @IBOutlet weak var labelLastDentalExam : UILabel!
    @IBOutlet      var arrayRadioButtons : [RadioButton]!
    @IBOutlet      var arraymedicationButtons : [UIButton]!
    @IBOutlet weak var labelMedicationOther : UILabel!
//    @IBOutlet      var arrayAllergyButtons : [UIButton]!
//    @IBOutlet weak var labelAllergyOthers : UILabel!
    @IBOutlet      var arrayTestButtons : [UIButton]!
    @IBOutlet      var arrayMedicalHistoryButtons1 : [UIButton]!
    @IBOutlet      var arrayMedicalHistoryButtons2 : [UIButton]!
    @IBOutlet weak var labelMedicalHistoryOther : UILabel!
    @IBOutlet      var arraySmokeButtons : [UIButton]!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var labelOfficeComments : [UILabel]!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelBloodPressure : UILabel!
   // @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var radioTest : RadioButton!
    @IBOutlet weak var labelSurgery : UILabel!
    
    @IBOutlet weak var viewAllergy : UIView!
    @IBOutlet weak var tableViewAllergy: UITableView!
    @IBOutlet weak var footerView1: UIView!
    @IBOutlet weak var footerView2: UIView!
    @IBOutlet weak var footerView3: UIView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
//    var arrayAllergies : NSMutableArray = NSMutableArray()

    var allergyArray: [Allergy] {
        get {
            if let patientDetails = patient.patientDetails {
                if patientDetails.allergySelected.count > 0 {
                    return patientDetails.allergySelected
                } else {
                    return patient.allergyArray
                }
            } else {
                return patient.allergyArray
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var allergyHeight: CGFloat = 0.0
        let cell = self.tableViewAllergy.dequeueReusableCell(withIdentifier: "Cell") as! AllergyTableViewCell
        for allergy in allergyArray {
            let height = allergy.allergyName.heightWithConstrainedWidth(cell.labelAllergyName.frame.width, font: cell.labelAllergyName.font) + 3
            allergyHeight = allergyHeight + height
        }
        tableViewAllergy.frame.size.height = allergyHeight
        viewAllergy.frame.size.height = tableViewAllergy.frame.minY + tableViewAllergy.frame.height + 5
        
        var y = viewAllergy.frame.maxY
        footerView1.frame.origin.y = (y < screenSize.height && y + footerView1.frame.height > screenSize.height) ? screenSize.height + 50 : y
        y = footerView1.frame.maxY
        
        footerView2.frame.origin.y = (y < screenSize.height && y + footerView2.frame.height > screenSize.height) ? screenSize.height + 50 : y
        y = footerView2.frame.maxY
        
        footerView3.frame.origin.y = (y < screenSize.height && y + footerView3.frame.height > screenSize.height) ? screenSize.height + 50 : y
        y = footerView3.frame.maxY
        
        constraintHeight.constant = CGFloat(Int(y/screenSize.height) + 2) * screenSize.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
        BRProgressHUD.show()
        
        var allergies = [String: String]()
        if patient.medicalHistory.radioAllergyTag == 1 {
            for allergy in allergyArray {
                if allergy.isSelected == true {
                    allergies[allergy.allergyId] = allergy.allergyDescription
                }
            }
        }
        
        var problems = [String]()
        for question in patient.medicalHistory.medicalQuestions[1] {
            if question.selectedOption == true {
                problems.append(question.question)
            }
        }
        
        for question in patient.medicalHistory.medicalQuestions[2] {
            if question.selectedOption == true {
                if question.isAnswerRequired == true {
                    problems.append(question.answer!)
                } else {
                    problems.append(question.question)
                }
            }
        }
        
        var questions = [String]()
        for question in patient.medicalHistory.medicalQuestions[0] {
            if question.selectedOption == true {
                questions.append(question.question)
            }
        }
        
        let values = ["Blood Thinners", "Steroids", "Nitroglycerin", "Bisphosphonates", "Other"]
        var medications = [String]()
        for tag in patient.medicalHistory.medicationsTag {
            if tag == 5 {
                medications.append(patient.medicalHistory.medicationOthers)
            } else {
                medications.append(values[tag - 1])
            }
        }
        
        let params = ["PatId" : patient.patientDetails!.patientNumber!, "AllergyList" : allergies, "medications" : medications.joined(separator: ","), "ProblemList" : problems, "medicalQuestions" : questions, "first_name" : patient.firstName!, "last_name": patient.lastName!] as NSDictionary
        Allergy.addAllergyList(params, success: { (result) in
            BRProgressHUD.hide()
            super.buttonActionSubmit(withSender: sender)
        }) { (error) in
            BRProgressHUD.hide()
            self.showAlert("Something went wrong. Please try again!")
        }
    }


    func loadData() {
        labelPatientName.text = patient.fullName
        labelBirthDate.text = patient.dateOfBirth
        labelBp.text = patient.medicalHistory.bloodpressureLow == "" && patient.medicalHistory.bloodpressureHigh == "" ? "N/A" : patient.medicalHistory.bloodpressureLow + " - " + patient.medicalHistory.bloodpressureHigh
        labelPulse.text = patient.medicalHistory.pulse
        labelPhysicianName.text = patient.medicalHistory.physicianName
        labelPhysicianPhone.text = patient.medicalHistory.PhysicianPhone
        labelPhysicianAddress.text = patient.medicalHistory.physicianAddress
        labelLastDentalExam.text = patient.medicalHistory.lastDentalExam
        for btn in arrayRadioButtons{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[0][btn.tag]
            btn.isSelected = ques.selectedOption == true
        }
        for btn in arraymedicationButtons{
            btn.isSelected = patient.medicalHistory.medicationsTag.contains(btn.tag)
        }
        labelMedicationOther.text = patient.medicalHistory.medicationOthers
//        for btn in arrayAllergyButtons{
//            btn.selected = patient.medicalHistory.arrayAllergyTags.contains(btn.tag)
//
//        }
//        labelAllergyOthers.text = patient.medicalHistory.allergyOthers
        for btn in arrayTestButtons{
            btn.isSelected = patient.medicalHistory.arrayTestTags.contains(btn.tag)
        }
        for btn in arrayMedicalHistoryButtons1{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[1][btn.tag]
            btn.isSelected = ques.selectedOption == true
        }
        for btn in arrayMedicalHistoryButtons2{
            let ques : PDQuestion = patient.medicalHistory.medicalQuestions[2][btn.tag]
            btn.isSelected = ques.selectedOption == true
        }

        labelMedicalHistoryOther.text = patient.medicalHistory.medicalQuestions[2][11].answer
        for btn in arraySmokeButtons{
            btn.isSelected = patient.medicalHistory.arraySmokeTag.contains(btn.tag)
        }
        radioPregnant.setSelectedWithTag(patient.medicalHistory.radioPregnantTag)
        radioBirthControl.setSelectedWithTag(patient.medicalHistory.radioBirthControlTag)
        textviewComments.text = patient.medicalHistory.patientComments
        patient.medicalHistory.medicalHistoryOfficeComments.setTextForArrayOfLabels(labelOfficeComments)
        signatureOffice.image = patient.medicalHistory.medicalHistoryOfficeSign
        signaturePatient.image = patient.medicalHistory.patientSignature
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.medicalHistory.medicalHistoryOfficeComments == "" ? "" : patient.dateToday
        
        labelSurgery.text = patient.medicalHistory.medicalQuestions[0][2].answer
        labelBloodPressure.text = patient.medicalHistory.medicalQuestions[0][9].answer
        radioTest.isSelected = patient.medicalHistory.arrayTestTags.count > 0
        viewAllergy.isHidden = patient.medicalHistory.radioAllergyTag == 2
        //radioAllergy.selected = patient.medicalHistory.arrayAllergyTags.count > 0
    }

}

extension MedicalHistoryFormViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allergyArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AllergyTableViewCell
        let allergy = self.allergyArray[indexPath.row]
        let height = allergy.allergyName.heightWithConstrainedWidth(cell.labelAllergyName.frame.width, font: cell.labelAllergyName.font) + 3
        return height
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AllergyTableViewCell
        //  cell.delegate = self
        cell.tag = indexPath.row
        cell.configureFormCell(self.allergyArray[indexPath.row])
        
        return cell
    }
    
}

