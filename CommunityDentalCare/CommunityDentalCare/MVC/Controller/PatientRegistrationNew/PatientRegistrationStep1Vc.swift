
//
//  PatientRegistrationStep1Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 27/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationStep1Vc: PDViewController {

    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet var textFieldPhoneNumber: MCTextField!
    @IBOutlet var textFieldAddress: MCTextField!
    @IBOutlet var dropdownRelation: BRDropDown!
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var labelDateOfBirth: UILabel!
    @IBOutlet weak var radioParent: RadioButton!
    @IBOutlet var labelpatientOrGuardian: UILabel!
    
    @IBOutlet var radioButtonNo: RadioButton!
    
    var isSecondGuardian : Bool = false
    var guardian2 : GuardianPage2!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropdownRelation.items = ["FATHER", "MOTHER", "LEGAL GUARDIAN"]
     
        dropdownRelation.placeholder = "-- RELATION *--"
        
        textFieldDate.textFormat = .date
        textFieldYear.textFormat = .year
        textFieldMonth.textFormat = .month
        textFieldPhoneNumber.textFormat = .phone
        if  patient.parentRadioButton != nil{
            radioParent.setSelectedWithTag(patient.parentRadioButton)
        }
        
        if radioParent.selected.tag == 1 {
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textFieldDate.isEnabled = true
            textFieldMonth.isEnabled = true
            textFieldYear.isEnabled = true
            textFieldPhoneNumber.isEnabled = true
            textFieldAddress.isEnabled = true
            dropdownRelation.isUserInteractionEnabled = true
            dropdownRelation.alpha = 1
            labelDateOfBirth.isEnabled = true
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldDate.text = ""
            textFieldMonth.text = ""
            textFieldYear.text = ""
            textFieldPhoneNumber.text = ""
            textFieldAddress.text = ""
            
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textFieldDate.isEnabled = false
            textFieldMonth.isEnabled = false
            textFieldYear.isEnabled = false
            labelDateOfBirth.isEnabled = false
            textFieldPhoneNumber.isEnabled = false
            textFieldAddress.isEnabled = false
            dropdownRelation.placeholder = "-- RELATION *--"
            dropdownRelation.isUserInteractionEnabled = false
            dropdownRelation.alpha = 0.5
        }
        //AutoFill
        
        textFieldFirstName.text = patient.parentFirstName
        textFieldLastName.text = patient.parentLastName
        textFieldPhoneNumber.text = patient.parentPhone
        textFieldAddress.text = patient.parentAddress
        if patient.parentDateOfBirth != nil {
            splitDateOfBirth(
                patient.parentDateOfBirth!)
        }
        
        if radioParent.isSelected &&  patient.parentRelationType  != "" {
            
          dropdownRelation.setSelectedOption = patient.parentRelationType
        }
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues () {
        self.patient.parentFirstName = self.radioParent.isSelected ? self.textFieldFirstName.text! : ""
        self.patient.parentLastName = self.radioParent.isSelected ? self.textFieldLastName.text! : ""
        self.patient.parentDateOfBirth = self.radioParent.isSelected ? self.getDateOfBirth() : ""
        self.patient.parentAddress = self.radioParent.isSelected ? self.textFieldAddress.text! : ""
        self.patient.parentPhone = self.radioParent.isSelected ? self.textFieldPhoneNumber.text! : ""
//        self.patient.parentRelationType = self.dropdownRelation.selectedOption
        self.patient.parentRadioButton = self.radioParent.selected.tag
        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
        
        if radioParent.isSelected &&  dropdownRelation.selectedOption  != nil {
            
           patient.parentRelationType =  dropdownRelation.selectedOption!
            
            
            
            
        }
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
       super.buttonActionBack(withSender: sender)
    }
    
    
    
    
    
    @IBAction func radioParentAction() {
        if radioParent.isSelected {
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textFieldDate.isEnabled = true
            textFieldMonth.isEnabled = true
            textFieldYear.isEnabled = true
            textFieldPhoneNumber.isEnabled = true
            textFieldAddress.isEnabled = true
            dropdownRelation.isUserInteractionEnabled = true
            dropdownRelation.alpha = 1
            labelDateOfBirth.isEnabled = true
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldDate.text = ""
            textFieldMonth.text = ""
            textFieldYear.text = ""
            textFieldPhoneNumber.text = ""
            textFieldAddress.text = ""
            
            
            
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textFieldDate.isEnabled = false
            textFieldMonth.isEnabled = false
            textFieldYear.isEnabled = false
            labelDateOfBirth.isEnabled = false
            textFieldPhoneNumber.isEnabled = false
            textFieldAddress.isEnabled = false
            dropdownRelation.placeholder = "-- RELATION *--"
            dropdownRelation.isUserInteractionEnabled = false
            dropdownRelation.alpha = 0.5
        }
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
        if radioParent.isSelected && (textFieldFirstName.isEmpty || textFieldLastName.isEmpty) {
            self.showAlert("PLEASE ENTER YOUR NAME")
        } else if !textFieldPhoneNumber.isEmpty && !textFieldPhoneNumber.text!.isPhoneNumber {
                self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else if radioParent.isSelected && dropdownRelation.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT RELATION")
        } else if radioParent.isSelected && invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            if radioParent.isSelected {
                YesOrNoAlert.sharedInstance.showWithTitle("DO YOU HAVE ANOTHER GUARDIAN", button1Title: "YES", button2Title: "NO", completion: { (buttonIndex) in
                    if buttonIndex == 1{
                        let newPatientStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistrationGuardian2Vc") as! PatientRegistrationGuardian2Vc
                        self.patient.parentFirstName = self.radioParent.isSelected ? self.textFieldFirstName.text! : ""
                        self.patient.parentLastName = self.radioParent.isSelected ? self.textFieldLastName.text! : ""
                        self.patient.parentDateOfBirth = self.radioParent.isSelected ? self.getDateOfBirth() : ""
                        self.patient.parentAddress = self.radioParent.isSelected ? self.textFieldAddress.text! : ""
                        self.patient.parentPhone = self.radioParent.isSelected ? self.textFieldPhoneNumber.text! : ""
                       self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                        self.patient.parentRadioButton = self.radioParent.selected.tag
                        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                        newPatientStep5VC.patient = self.patient
               self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                    }else{
                        self.patient.parentFirstName = self.radioParent.isSelected ? self.textFieldFirstName.text! : ""
                        self.patient.parentLastName = self.radioParent.isSelected ? self.textFieldLastName.text! : ""
                        self.patient.parentDateOfBirth = self.radioParent.isSelected ? self.getDateOfBirth() : ""
                        self.patient.parentAddress = self.radioParent.isSelected ? self.textFieldAddress.text! : ""
                        self.patient.parentPhone = self.radioParent.isSelected ? self.textFieldPhoneNumber.text! : ""
                        self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                        self.patient.parentRadioButton = self.radioParent.selected.tag
                        self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                        let newPatientStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistrationStep2Vc") as! PatientRegistrationStep2Vc
                        newPatientStep5VC.patient = self.patient
                        self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                    }
                })
            }else{
                let newPatientStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistrationStep2Vc") as! PatientRegistrationStep2Vc
                self.patient.parentFirstName = self.radioParent.isSelected ? self.textFieldFirstName.text! : ""
                self.patient.parentLastName = self.radioParent.isSelected ? self.textFieldLastName.text! : ""
                self.patient.parentDateOfBirth = self.radioParent.isSelected ? self.getDateOfBirth() : ""
                self.patient.parentAddress = self.radioParent.isSelected ? self.textFieldAddress.text! : ""
                self.patient.parentPhone = self.radioParent.isSelected ? self.textFieldPhoneNumber.text! : ""
                if radioParent.isSelected &&  dropdownRelation.selectedOption  != nil {
                    
                    patient.parentRelationType =  dropdownRelation.selectedOption!
                }
//                self.patient.parentRelationType = self.dropdownRelation.selectedOption!
                self.patient.parentRadioButton = self.radioParent.selected.tag
                self.patient.parentRelationType2 = self.dropdownRelation.selectedIndex
                newPatientStep5VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
                
            }
        }
    }
    
    func  splitDateOfBirth(_ datebirth : String)  {
        let arrayString = datebirth.components(separatedBy: " ")
        if arrayString.count == 3{
            textFieldMonth.text = arrayString[0]
            textFieldDate.text = arrayString[1].replacingOccurrences(of: ",", with: "")
            textFieldYear.text = arrayString[2]
        }
        
    }
    func getDateOfBirth() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        if !textFieldDate.isEmpty && !textFieldMonth.isEmpty && !textFieldYear.isEmpty{
        let dob = dateFormatter.date(from: textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.string(from: dob)
    }
     return ""
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}

