//
//  PatientRegistrationStep3Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 27/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationStep3Vc: PDViewController {

    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldWorkPhone: MCTextField!
    @IBOutlet weak var textFieldCellPhone: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet var radioButtonPreferredMethod: RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldPhone.textFormat = .phone
        textFieldWorkPhone.textFormat = .phone
        textFieldCellPhone.textFormat = .phone
        textFieldEmail.textFormat = .email
        // Do any additional setup after loading the view.
        
        if patient.patientDetails != nil && patient.phone == ""{
            textFieldPhone.text = patient.patientDetails?.homePhone
        }else{
        //Autofill
            if patient.preferredMethod != nil{
                radioButtonPreferredMethod.setSelectedWithTag(patient.preferredMethod)
            }

        textFieldPhone.text = patient.phone
        textFieldCellPhone.text = patient.cellPhone
        textFieldEmail.text = patient.email
        textFieldWorkPhone.text = patient.workPhone
        }
        
        
    }
    
    func saveValues () {
        if radioButtonPreferredMethod.selected != nil {
            patient.preferredMethod = radioButtonPreferredMethod.selected.tag
            
        }

        patient.phone = textFieldPhone.isEmpty ? "" : textFieldPhone.text!
        patient.workPhone = textFieldWorkPhone.isEmpty ? "" : textFieldWorkPhone.text!
        patient.cellPhone = textFieldCellPhone.isEmpty ? "" : textFieldCellPhone.text!
        patient.email = textFieldEmail.isEmpty ? "" : textFieldEmail.text!
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }
    
    @IBAction func preferredContactMethod (withSender sender : UIButton){
        if sender.tag == 1{
            textFieldPhone.placeholder = "PHONE NUMBER *"
            textFieldCellPhone.placeholder = "CELL PHONE"
            textFieldWorkPhone.placeholder = "WORK PHONE"
            textFieldEmail.placeholder = "EMAIL ID"
        }else if sender.tag == 2{
            textFieldPhone.placeholder = "PHONE NUMBER"
            textFieldCellPhone.placeholder = "CELL PHONE *"
            textFieldWorkPhone.placeholder = "WORK PHONE"
            textFieldEmail.placeholder = "EMAIL ID"
            
        }else if sender.tag == 3{
            textFieldPhone.placeholder = "PHONE NUMBER"
            textFieldCellPhone.placeholder = "CELL PHONE"
            textFieldWorkPhone.placeholder = "WORK PHONE *"
            textFieldEmail.placeholder = "EMAIL ID"
            
        }else{
            
            textFieldPhone.placeholder = "PHONE NUMBER"
            textFieldCellPhone.placeholder = "CELL PHONE"
            textFieldWorkPhone.placeholder = "WORK PHONE"
            textFieldEmail.placeholder = "EMAIL ID *"
        }
    }

    @IBAction func buttonNextAction() {
        if radioButtonPreferredMethod.selected == nil {
            let alert = Extention.alert("PLEASE SELECT ANY ONE OF THE PREFERRED METHOD")
            self.present(alert, animated: true, completion: nil)
        }else if  (radioButtonPreferredMethod.selected.tag == 1 && textFieldPhone.isEmpty) || (!textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber) {
            self.showAlert("PLEASE ENTER A VALID PHONE NUMBER")
        } else if (radioButtonPreferredMethod.selected.tag == 3 && textFieldWorkPhone.isEmpty) || (!textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber) {
            self.showAlert("PLEASE ENTER A VALID WORK PHONE NUBMER")
        } else if (radioButtonPreferredMethod.selected.tag == 2 && textFieldCellPhone.isEmpty) || (!textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber) {
            self.showAlert("PLEASE ENTER A VALID CELL PHONE NUMBER")
        } else if (radioButtonPreferredMethod.selected.tag == 4 && textFieldEmail.isEmpty) || (!textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail) {
            self.showAlert("PLEASE ENTER A VALID EMAIL ID")
        } else {
            patient.phone = textFieldPhone.isEmpty ? "" : textFieldPhone.text!
            patient.workPhone = textFieldWorkPhone.isEmpty ? "" : textFieldWorkPhone.text!
            patient.cellPhone = textFieldCellPhone.isEmpty ? "" : textFieldCellPhone.text!
            patient.email = textFieldEmail.isEmpty ? "" : textFieldEmail.text!
            patient.preferredMethod = radioButtonPreferredMethod.selected.tag

            let step2EmergencyVC = patientStoryboard.instantiateViewController(withIdentifier: "PatientRegistrationStep4Vc") as! PatientRegistrationStep4Vc
            step2EmergencyVC.patient = self.patient
            self.navigationController?.pushViewController(step2EmergencyVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
