//
//  PatientRegistration6Vc.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistration6Vc: PDViewController {
    
    
    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    @IBOutlet weak var labelSecondaryInsurance: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  patient.radiobuttonTag != nil{
            radioPrimary.setSelectedWithTag(patient.radiobuttonTag)
            if patient.radiobuttonTag == 2 {
                self.setEnabled(false)
                radioSecondary.deselectAllButtons()
            }
        }
        
        if  patient.radiobuttonTag2 != nil{
            radioSecondary.setSelectedWithTag(patient.radiobuttonTag2)
        }
        if patient.radiobuttonTag == nil && patient.radiobuttonTag2 == nil {
            self.setEnabled(false)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func setEnabled(_ enabled: Bool) {
        for button in radioSecondary.groupButtons {
            (button as! RadioButton).isUserInteractionEnabled = enabled
            (button as! RadioButton).alpha = enabled ? 1.0 : 0.4
        }
        labelSecondaryInsurance.alpha = enabled ? 1.0 : 0.4
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues () {
        
    }
    
    
    
    @IBAction override func buttonActionBack(withSender sender: UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        
        
    }

    
    
    @IBAction func RadioButtonAction(withsender sender: RadioButton) {
        patient.radiobuttonTag = sender.selected.tag
        self.setEnabled(sender == radioPrimary)
        if sender != radioPrimary {
            radioSecondary.deselectAllButtons()
        }
        
    }
    
    @IBAction func RadioButtonAction2(withsender sender: RadioButton) {
        patient.radiobuttonTag2 = sender.selected.tag
        
    }
    
    
    @IBAction func buttonNextAction() {
        if radioPrimary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else if radioSecondary.isUserInteractionEnabled && radioSecondary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED")
        } else {
            if patient.radiobuttonTag == 1 {
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistration7Vc") as! PatientRegistration7Vc
                
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
            } else if radioSecondary.isUserInteractionEnabled && patient.radiobuttonTag2 == 3 {
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistration8Vc") as! PatientRegistration8Vc
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
            } else {
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "PatientRegistration9Vc") as! PatientRegistration9Vc
                step3VC.patient = self.patient
                self.navigationController?.pushViewController(step3VC, animated: true)
                
            }
        }
    }
}
