//
//  PatientRegistrationForm.swift
//  CommunityDentalCare
//
//  Created by SRS Web Solutions on 28/09/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PatientRegistrationForm: PDViewController {
    
    @IBOutlet var labelPatientLastName: FormLabel!
    @IBOutlet var labelPatientFirstName: FormLabel!
    @IBOutlet var labelMiddleInitial: FormLabel!
    @IBOutlet var labelpatientDOB: FormLabel!
    @IBOutlet var labelPatientAddress: FormLabel!
    @IBOutlet var labelPatientCity: FormLabel!
    @IBOutlet var labelPatientState: FormLabel!
    @IBOutlet var labelPatientZip: FormLabel!
    @IBOutlet var labelPatientHomeNo: FormLabel!
    @IBOutlet var labelPatientCellNo: FormLabel!
    @IBOutlet var labelPatientWorkNo: FormLabel!
    @IBOutlet var labelPatientEmail: FormLabel!
    @IBOutlet var radioButtonGender: RadioButton!
    @IBOutlet var radiobuttonPreferred: RadioButton!
    @IBOutlet var radiobuttonLanguage: RadioButton!
    @IBOutlet var labelLanguageOther: FormLabel!
    @IBOutlet var radioButtonRace: RadioButton!
    @IBOutlet var labelOtherRace: FormLabel!
    @IBOutlet var radioButtonEthnicity: RadioButton!
    @IBOutlet var labelEthnicityOther: FormLabel!
    @IBOutlet var radioButtonMaritalStatus: RadioButton!
    @IBOutlet var radioButtonReferredBy: RadioButton!
    @IBOutlet var labelOtherReferred: FormLabel!
    
    //Guardian Info 
    
    @IBOutlet var labelGuardianName: FormLabel!
    @IBOutlet var labelGuardianName2: FormLabel!
    @IBOutlet var labelGuardian1DOB: FormLabel!
    @IBOutlet var labelGuardian2DOB: FormLabel!
    @IBOutlet var labelGuardian1Phone: FormLabel!
    @IBOutlet var labelGuardian2Phone: FormLabel!
    @IBOutlet var labelGuardian1Address: FormLabel!
    @IBOutlet var labelGuardian2Address: FormLabel!
    @IBOutlet var labelGuardian1Relation: FormLabel!
    @IBOutlet var labelGuardian2Relation: FormLabel!
    
    //Emergency Contact
    
    @IBOutlet var labelEmergencyLastName: FormLabel!
    @IBOutlet var labelEmergencyFirstName: FormLabel!
    @IBOutlet var labelEmergencyPhone: FormLabel!
    @IBOutlet var labelEmergencyRelation: FormLabel!
    
    //Primary Insurance Information
    
    @IBOutlet var labelInsuranceFullName: FormLabel!
    @IBOutlet var labelInsuranceDOB: FormLabel!
    @IBOutlet var labelInsurancePlanName: FormLabel!
    @IBOutlet var labelInsurancePlanId: FormLabel!
    @IBOutlet var labelInsuranceGroupId: FormLabel!
    @IBOutlet var labelInsurancePhone: FormLabel!
    @IBOutlet var labelInsuranceAddress: FormLabel!
    
    //Secondary Insurance Information
    
    @IBOutlet var labelSecInsuranceFullName: FormLabel!
    @IBOutlet var labelSecInsuranceDOB: FormLabel!
    @IBOutlet var labelSecInsurancePlanName: FormLabel!
    @IBOutlet var labelSecInsurancePlanId: FormLabel!
    @IBOutlet var labelSecInsuranceGroupId: FormLabel!
    @IBOutlet var labelSecInsurancePhone: FormLabel!
    @IBOutlet var labelSecInsuranceAddress: FormLabel!

    //Witness
    
    @IBOutlet var labelWitnessName: FormLabel!
    @IBOutlet var labelWitnessPhone: FormLabel!
    
    
    @IBOutlet var imageViewSignature: UIImageView!
    @IBOutlet var labelDate: FormLabel!
    
    @IBOutlet var radiobuttonMinor: RadioButton!
    
    ////PRIVACY PRACTICE
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelPhoneNumber : UILabel!
    @IBOutlet weak var representativeName : UILabel!
    @IBOutlet weak var representativeRelationship : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var signature : UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        if patient.is18YearsOld{
        radiobuttonMinor.setSelectedWithTag(2)
        } else {
            radiobuttonMinor.setSelectedWithTag(1)
        }
        
        labelPatientFirstName.text = patient.firstName
        labelPatientLastName.text = patient.lastName
      //  labelMiddleInitial.text = patient.initial
        labelpatientDOB.text = patient.dateOfBirth
        labelPatientAddress.text = patient.address
        labelPatientCity.text = patient.city
        labelPatientState.text = patient.state
        labelPatientZip.text = patient.zip
        labelPatientHomeNo.text = patient.phone
        labelPatientWorkNo.text = patient.workPhone
        labelPatientCellNo.text = patient.cellPhone
        labelPatientEmail.text = patient.email
        radioButtonGender.setSelectedWithTag(patient.gender + 1)
        radiobuttonPreferred.setSelectedWithTag(patient.preferredMethod)
        radiobuttonLanguage.setSelectedWithTag(patient.languageDropDown)
        labelLanguageOther.text = patient.languageDropDown == 2 ? patient.languageOther : ""
        radioButtonRace.setSelectedWithTag(patient.raceDropDown)
        labelOtherRace.text = patient.raceDropDown == 7 ? patient.raceOther : ""
        radioButtonMaritalStatus.setSelectedWithTag(patient.maritalStatus + 1)
        radioButtonEthnicity.setSelectedWithTag(patient.ethnicityDropDown)
        labelEthnicityOther.text = patient.ethnicityDropDown == 5 ? patient.ethnicityOther : ""
        radioButtonReferredBy.setSelectedWithTag(patient.referralDropDown)
        labelOtherReferred.text = patient.referralDropDown == 15 ? patient.referredOther : ""
        
        //Guardian Info
        
        labelGuardianName.text = patient.parentFirstName + " " + patient.parentLastName
        labelGuardian1DOB.text = patient.parentDateOfBirth
        labelGuardian1Phone.text = patient.parentPhone
        labelGuardian1Address.text = patient.parentAddress
        labelGuardian1Relation.text = patient.parentRelationType
        
        labelGuardianName2.text = patient.guardian2.firstName + " " + patient.guardian2.lastName
        labelGuardian2DOB.text = patient.guardian2.dateOfBirth
        labelGuardian2Phone.text = patient.guardian2.phoneNumber
        labelGuardian2Address.text = patient.guardian2.address
        labelGuardian2Relation.text = patient.guardian2.relation
        
        //Emergency Contact
        
        labelEmergencyFirstName.text = patient.emergencyFirstName
        labelEmergencyLastName.text = patient.emergencyLastName
        labelEmergencyPhone.text = patient.emergencyPhone
        
        
//        if patient.emergencyRelationOther == "" {
//        
//        labelEmergencyRelation.text = patient.emergencyRelation
//        }else {
        
        labelEmergencyRelation.text = patient.emergencyRelationOther
//        }
        //Primary Insurance Information

        labelInsuranceFullName.text = patient.primaryInsurance.policyholderName
        labelInsurancePlanName.text = patient.primaryInsurance.insuredPlanName
        labelInsuranceAddress.text = patient.primaryInsurance.address
        labelInsuranceGroupId.text = patient.primaryInsurance.groupId
        labelInsurancePlanId.text = patient.primaryInsurance.policyId
        labelInsurancePhone.text = patient.primaryInsurance.phone
        labelInsuranceDOB.text = patient.primaryInsurance.dateOfBirth
        
        //Secondary Insurance Information
        
        labelSecInsuranceFullName.text = patient.secondaryInsurance.policyholderName
        labelSecInsurancePlanName.text = patient.secondaryInsurance.insuredPlanName
        labelSecInsuranceAddress.text = patient.secondaryInsurance.address
        labelSecInsuranceGroupId.text = patient.secondaryInsurance.groupId
        labelSecInsurancePlanId.text = patient.secondaryInsurance.policyId
        labelSecInsurancePhone.text = patient.secondaryInsurance.phone
        labelSecInsuranceDOB.text = patient.secondaryInsurance.dateOfBirth

        //Witness
        
        labelWitnessName.text = patient.witnessName
        labelWitnessPhone.text = patient.witnessPhone
        imageViewSignature.image = patient.signature
        labelDate.text = patient.dateToday

        
        ////PRIVACY PRACTICE
        labelPatientName.text = patient.fullName
        labelAddress.text = patient.address + ", " + patient.city + ", " + patient.state + ", " + patient.zip
        labelPhoneNumber.text = patient.cellPhone
        representativeName.text = patient.privacyRepresentativeName
        representativeRelationship.text = patient.privacyRepresentativeRelationship
        labelDate1.text = patient.dateToday
        signature.image = patient.privacySignature

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Community Dental Care", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            return
        }
        BRProgressHUD.show()
        ServiceManager.sendPatientDetails(patient, completion: { (result, error) in
            BRProgressHUD.hide()
            if result {
                super.buttonActionSubmit(withSender: sender)
            } else {
                let alert = Extention.alert(error!.localizedDescription.uppercased())
                self.present(alert, animated: true, completion: nil)
            }
        })
    }


}
