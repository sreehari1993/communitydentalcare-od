//
//  ChildMedicalHistory1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistory1ViewController: PDViewController {

    @IBOutlet weak var textfieldPhysicianName : UITextField!
    @IBOutlet weak var textfieldAddress : UITextField!
    @IBOutlet weak var textfieldPhone : UITextField!
    @IBOutlet weak var textfieldLowBp : UITextField!
    @IBOutlet weak var textfieldHighBp : UITextField!
    @IBOutlet weak var radioAllergy : RadioButton!

    @IBOutlet weak var textfieldPulse : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.childMedicalHistory.physicianName = textfieldPhysicianName.isEmpty ? "N/A" : textfieldPhysicianName.text!
        patient.childMedicalHistory.physicianAddress = textfieldAddress.isEmpty ? "N/A" : textfieldAddress.text!
        patient.childMedicalHistory.PhysicianPhone = textfieldPhone.isEmpty ? "N/A" : textfieldPhone.text!
        patient.childMedicalHistory.bloodpressureLow = textfieldLowBp.isEmpty ? "" : textfieldLowBp.text!
        patient.childMedicalHistory.bloodpressureHigh = textfieldHighBp.isEmpty ? "" : textfieldHighBp.text!

        patient.childMedicalHistory.pulse = textfieldPulse.isEmpty ? "N/A" : textfieldPulse.text!
        patient.childMedicalHistory.allergyTag = radioAllergy.selected == nil ? 0 : radioAllergy.selected.tag
    }
    
    func loadValues()  {
        textfieldPhysicianName.setSavedText(patient.childMedicalHistory.physicianName)
        textfieldAddress.setSavedText(patient.childMedicalHistory.physicianAddress)
        textfieldPhone.setSavedText(patient.childMedicalHistory.PhysicianPhone)
        textfieldHighBp.setSavedText(patient.childMedicalHistory.bloodpressureHigh)
        textfieldLowBp.setSavedText(patient.childMedicalHistory.bloodpressureLow)
        textfieldPulse.setSavedText(patient.childMedicalHistory.pulse)
        radioAllergy.setSelectedWithTag(patient.childMedicalHistory.allergyTag)
        
        if let patientDetails = patient.patientDetails {
            if patientDetails.allergySelected.count > 0 {
                radioAllergy.isSelected = true
            }
        }
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if radioAllergy.selected == nil{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)

        }else if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            let alert = Extention.alert("PLEASE ENTER THE VALID PHONE NUMBER")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            saveValues()
            if radioAllergy.selected.tag == 1{
                let allergy = medicalStoryboard.instantiateViewController(withIdentifier: "AllergyVC") as! AllergyViewController
                allergy.patient = self.patient
                self.navigationController?.pushViewController(allergy, animated: true)
            }else{
                let medical = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildMedical2VC") as! ChildMedicalHistory2ViewController
                medical.patient = self.patient
                self.navigationController?.pushViewController(medical, animated: true)
            }
        }
    }

}

extension ChildMedicalHistory1ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhone {
            return textField.formatPhoneNumber(range, string: string)
        }else if textField == textfieldLowBp || textField == textfieldPulse || textField == textfieldHighBp{
            return textField.formatNumbers(range, string: string, count: 3)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

