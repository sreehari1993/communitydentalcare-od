//
//  ChildMedicalHistoryFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildMedicalHistoryFormViewController: PDViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!
    @IBOutlet weak var labelBp : UILabel!
    @IBOutlet weak var labelPulse : UILabel!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianTelephone : UILabel!
    @IBOutlet weak var labelPhysicianAddress : UILabel!
    @IBOutlet      var arrayRadioButtons : [RadioButton]!
//    @IBOutlet      var arrayAllergyButtons : [UIButton]!
//    @IBOutlet weak var labelAllergyOthers : UILabel!
    @IBOutlet      var arrayTestButtons : [UIButton]!
    @IBOutlet      var arrayMedicalButtons1 : [UIButton]!
    @IBOutlet      var arrayMedicalButtons2 : [UIButton]!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var labelOfficeComments : [UILabel]!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
//    @IBOutlet weak var radioAllergy : RadioButton!
    @IBOutlet weak var radioTest : RadioButton!
    
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var labelDateOfBirth1 : UILabel!
    @IBOutlet var buttonTechniques : [RadioButton]!
    @IBOutlet weak var signatureParent : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    @IBOutlet weak var labelDate5 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    
    @IBOutlet weak var heightConstraint : NSLayoutConstraint!
    @IBOutlet weak var viewconsent1 : UIView!
    @IBOutlet weak var viewConsent2 : UIView!
    @IBOutlet weak var signatureParent1 : UIImageView!
    @IBOutlet weak var labelDate6 : UILabel!
    @IBOutlet weak var viewSignature1 : UIView!
    
    var allergyArray: [Allergy] {
        get {
            if let patientDetails = patient.patientDetails {
                if patientDetails.allergySelected.count > 0 {
                    return patientDetails.allergySelected
                } else {
                    return patient.allergyArray
                }
            } else {
                return patient.allergyArray
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
//        getAllergyValue()
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        var allergyHeight: CGFloat = 0.0
//        let cell = self.tableViewAllergy.dequeueReusableCell(withIdentifier: "Cell") as! AllergyTableViewCell
//        for allergy in allergyArray {
//            let height = allergy.allergyName.heightWithConstrainedWidth(cell.labelAllergyName.frame.width, font: cell.labelAllergyName.font) + 3
//            allergyHeight = allergyHeight + height
//        }
//        tableViewAllergy.frame.size.height = allergyHeight
//        viewAllergy.frame.size.height = tableViewAllergy.frame.minY + tableViewAllergy.frame.height + 5
//        
//        var y = viewAllergy.frame.maxY
//        footerView1.frame.origin.y = (y < screenSize.height && y + footerView1.frame.height > screenSize.height) ? screenSize.height + 50 : y
//        y = footerView1.frame.maxY
//        
//        footerView2.frame.origin.y = (y < screenSize.height && y + footerView2.frame.height > screenSize.height) ? screenSize.height + 50 : y
//        y = footerView2.frame.maxY
//        
//        footerView3.frame.origin.y = (y < screenSize.height && y + footerView3.frame.height > screenSize.height) ? screenSize.height + 50 : y
//        y = footerView3.frame.maxY
//        
//        constraintHeight.constant = CGFloat(Int(y/screenSize.height) + 2) * screenSize.height
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
        BRProgressHUD.show()
        
        var allergies = [String: String]()
        if patient.childMedicalHistory.allergyTag == 1 {
            for allergy in allergyArray {
                if allergy.isSelected == true {
                    allergies[allergy.allergyId] = allergy.allergyDescription
                }
            }
        }
        
        var problems = [String]()
        for question in patient.childMedicalHistory.medicalQuestions[1] {
            if question.selectedOption == true {
                problems.append(question.question)
            }
        }
        
        for question in patient.childMedicalHistory.medicalQuestions[2] {
            if question.selectedOption == true {
                if question.isAnswerRequired == true {
                    problems.append(question.answer!)
                } else {
                    problems.append(question.question)
                }
            }
        }
        
        
        let params = ["PatId" : patient.patientDetails!.patientNumber , "AllergyList" : allergies, "ProblemList" : problems,"first_name" : patient.firstName, "last_name": patient.lastName] as NSDictionary
        Allergy.addAllergyList(params, success: { (result) in
            BRProgressHUD.hide()
            super.buttonActionSubmit(withSender: sender)
        }) { (error) in
            BRProgressHUD.hide()
            self.showAlert("Something went wrong. Please try again!")
        }
    }

    func loadData(){
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        labelBp.text = patient.childMedicalHistory.bloodpressureLow == "" && patient.childMedicalHistory.bloodpressureHigh == "" ? "N/A" : patient.childMedicalHistory.bloodpressureLow + " - " + patient.childMedicalHistory.bloodpressureHigh
        labelPulse.text = patient.childMedicalHistory.pulse
        labelPhysicianName.text = patient.childMedicalHistory.physicianName
        labelPhysicianTelephone.text = patient.childMedicalHistory.PhysicianPhone
        labelPhysicianAddress.text = patient.childMedicalHistory.physicianAddress
        for btn in arrayRadioButtons{
            btn.isSelected = patient.childMedicalHistory.medicalQuestions[0][btn.tag].selectedOption == true
        }
//        for btn in arrayAllergyButtons{
//            btn.selected = patient.childMedicalHistory.arrayAllergyTags.contains(btn.tag)
//        }
//        labelAllergyOthers.text = patient.childMedicalHistory.allergyOthers
        
        for btn in arrayTestButtons{
            btn.isSelected = patient.childMedicalHistory.arrayTestTags.contains(btn.tag)

        }
        for btn in arrayMedicalButtons1{
            btn.isSelected = patient.childMedicalHistory.medicalQuestions[1][btn.tag].selectedOption == true
        }
        for btn in arrayMedicalButtons2{
            btn.isSelected = patient.childMedicalHistory.medicalQuestions[1][btn.tag].selectedOption == true
        }

        textviewComments.text = patient.childMedicalHistory.patientComments
        patient.childMedicalHistory.childMedicalHistoryOfficeComments.setTextForArrayOfLabels(labelOfficeComments)
        signaturePatient.image = patient.childMedicalHistory.patientSignature
        signatureOffice.image = patient.childMedicalHistory.childMedicalHistoryOfficeSign
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.childMedicalHistory.childMedicalHistoryOfficeComments == "" ? "" : patient.dateToday
//        radioAllergy.setSelected(patient.childMedicalHistory.arrayAllergyTags.count > 0)
        radioTest.setSelected(patient.childMedicalHistory.arrayTestTags.count > 0)
        
        
        labelPatientName1.text = patient.fullName
        labelDateOfBirth1.text = patient.dateOfBirth
        signatureParent.image = patient.informedSignatureParent
        signatureParent1.image = patient.informedSignatureParent

        signatureWitness.image = patient.informedSignatureWitness
        signatureDoctor.image = patient.informedSignatureDoctor
        for btn in buttonTechniques{
            btn.isSelected = patient.informedConsentTechniqueTag.contains(btn.tag)
        }
        labelDate5.text = patient.dateToday
        labelDate6.text = patient.dateToday
        labelDate4.text = patient.dateToday
        labelDate3.text = patient.dateToday
        
        if patient.childMedicalHistory.allergyTag == 1{
            viewSignature1.isHidden = true
            viewconsent1.frame = CGRect(x: 0, y: 2048, width: screenSize.width, height: 1024)
            viewConsent2.frame = CGRect(x: 0, y: 3072, width: screenSize.width, height: 1024)
            heightConstraint.constant = 4096
        }
    }
}

extension ChildMedicalHistoryFormViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allergyArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AllergyTableViewCell
        let allergy = self.allergyArray[indexPath.row]
        let height = allergy.allergyName.heightWithConstrainedWidth(cell.labelAllergyName.frame.width, font: cell.labelAllergyName.font) + 3
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AllergyTableViewCell
        //  cell.delegate = self
        cell.tag = indexPath.row
        cell.configureFormCell(allergyArray[indexPath.row])
        
        return cell
    }
    
}

