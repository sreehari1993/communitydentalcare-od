//
//  ChildDentalHistory3ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildDentalHistory3ViewController: PDViewController {

    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        loadValues()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.childDentalHistory.patientComments = textviewComments.text! == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text!
        
    }
    
    func loadValues()  {
        textviewComments.text = patient.childDentalHistory.patientComments == "N/A" ? "PLEASE TYPE HERE" : patient.childDentalHistory.patientComments
        if textviewComments.text == "PLEASE TYPE HERE"{
            textviewComments.textColor = UIColor.lightGray
        }else{
            textviewComments.textColor = UIColor.black
        }
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
            
        }else if !labelDate.dateTapped{
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
            
        }else{
            saveValues()
            patient.childDentalHistory.patientSignature = signaturePatient.signatureImage()
            let medical = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildDentalFormVC") as! ChildDentalFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }
        
    }


}

extension ChildDentalHistory3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

