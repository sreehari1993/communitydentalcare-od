//
//  ChildDentalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/29/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ChildDentalHistory2ViewController: PDViewController {

    @IBOutlet weak var textviewReason : UITextView!
    @IBOutlet weak var textfieldTreatment : UITextField!
    @IBOutlet weak var textfieldTreatmentDate: UITextField!
    @IBOutlet weak var radioFear : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        DateInputView.addDatePickerForTextField(textfieldTreatmentDate)
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.childDentalHistory.reasonVisit = textviewReason.text! == "PLEASE TYPE HERE" ? "" : textviewReason.text!
        patient.childDentalHistory.treatment = textfieldTreatment.isEmpty ? "N/A" : textfieldTreatment.text!
        patient.childDentalHistory.treatmentDate = textfieldTreatmentDate.isEmpty ? "N/A" : textfieldTreatmentDate.text!
        patient.childDentalHistory.radioFearTag = radioFear.selected == nil ? 0 : radioFear.selected.tag
        
    }
    
    func loadValues()  {
        textviewReason.text = patient.childDentalHistory.reasonVisit == "" ? "PLEASE TYPE HERE" : patient.childDentalHistory.reasonVisit
        if textviewReason.text == "PLEASE TYPE HERE"{
            textviewReason.textColor = UIColor.lightGray
        }else{
            textviewReason.textColor = UIColor.black
        }
        textfieldTreatment.setSavedText(patient.childDentalHistory.treatment)
        textfieldTreatmentDate.setSavedText(patient.childDentalHistory.treatmentDate)
        radioFear.setSelectedWithTag(patient.childDentalHistory.radioFearTag)
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textviewReason.text == "PLEASE TYPE HERE"{
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)

        }else{
            saveValues()
            let dental = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildDental3VC") as! ChildDentalHistory3ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }
        
    }
    

}
extension ChildDentalHistory2ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension ChildDentalHistory2ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
