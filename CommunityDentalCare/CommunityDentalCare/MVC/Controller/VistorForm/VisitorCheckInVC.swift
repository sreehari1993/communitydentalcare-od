//
//  ParentInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class VisitorCheckinVC: PDViewController {
  
    @IBOutlet weak var labelDate: UILabel!
 
    @IBOutlet weak var textFieldFirstName: PDTextField!
    @IBOutlet weak var textFieldLastName: PDTextField!
    @IBOutlet weak var purposeOfVisit: PDTextField!
 

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = patient.dateToday
               // Do any additional setup after loading the view.
        
        PickerPurposeOfVisit.addPickVisitorForTextField(purposeOfVisit)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        func gotoAddressInfoVC() {
         
            let hippaInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kVisitorNewHippaVC") as! VisitorCheckInNewHippaVC
            hippaInfoVC.patient = patient
            self.navigationController?.pushViewController(hippaInfoVC, animated: true)
        }
        
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT FIRST NAME")
            self.present(alert, animated: true, completion: nil)
        } else if textFieldLastName.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PATIENT LAST NAME")
            self.present(alert, animated: true, completion: nil)
        }else if purposeOfVisit.isEmpty {
            let alert = Extention.alert("PLEASE ENTER PURPOSE OF VISIT")
            self.present(alert, animated: true, completion: nil)
        }else{
            
            patient.VisitorFullName = textFieldFirstName.text! + " " + textFieldLastName.text!
            patient.VisitingPurpose = purposeOfVisit.text
            patient.firstName = textFieldFirstName.text!
            patient.lastName = textFieldLastName.text!
            
            gotoAddressInfoVC()
        }
    }
    

    
    

  

}

class PickerPurposeOfVisit: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    
    
    var arrayMonths = ["PATIENT","VENDOR","OTHERS"]
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1
        textField.resignFirstResponder()
    }
    
    class func addPickVisitorForTextField(_ textField: UITextField) {
        let monthListView = PickerPurposeOfVisit(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension PickerPurposeOfVisit : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //  let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        // textField.text = string1.substringWithRange(string1.startIndex ..< string1.startIndex.advancedBy(3))
        
        textField.text = arrayMonths[row]
    }
}

