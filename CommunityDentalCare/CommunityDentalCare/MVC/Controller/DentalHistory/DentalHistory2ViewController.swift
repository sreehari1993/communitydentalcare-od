//
//  DentalHistory2ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistory2ViewController: PDViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            let alert = Extention.alert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
            self.present(alert, animated: true, completion: nil)
        } else {
            let dental = medicalStoryboard.instantiateViewController(withIdentifier: "Dental3VC") as! DentalHistory3ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
            }
        }


}

extension DentalHistory2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.dentalHistory.dentalQuestions.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(self.patient.dentalHistory.dentalQuestions[indexPath.row])
        
        return cell
    }
}

extension DentalHistory2ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(_ cell: PatientInfoCell) {
        PopupTextField.sharedInstance.showWithPlaceHolder("WHICH SIDE?", keyboardType: UIKeyboardType.default, textFormat: TextFormat.default) { (textField, isEdited) in
            if isEdited{
                self.patient.dentalHistory.dentalQuestions[cell.tag].answer = textField.text
            }else{
                cell.radioButtonYes.isSelected = false
            }

        }
    }
}

