//
//  DentalHistory1ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistory1ViewController: PDViewController {
    @IBOutlet weak var textviewReason : UITextView!
    @IBOutlet weak var radioToothBrush : RadioButton!
    @IBOutlet weak var textfieldDentalCheckup : UITextField!
    @IBOutlet weak var textfieldXray : UITextField!
    @IBOutlet weak var textfieldExam : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        DateInputView.addDatePickerForTextField(textfieldDentalCheckup)
        DateInputView.addDatePickerForTextField(textfieldXray)
        DateInputView.addDatePickerForTextField(textfieldExam)

        loadValues()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveValues (){
        patient.dentalHistory.dentalReason = textviewReason.text!
        patient.dentalHistory.toothBrushTag = radioToothBrush.selected == nil ? 0 : radioToothBrush.selected.tag
        patient.dentalHistory.dentalCheckup = textfieldDentalCheckup.isEmpty ? "N/A" : textfieldDentalCheckup.text!
        patient.dentalHistory.dentalXray = textfieldXray.isEmpty ? "N/A" : textfieldXray.text!
        patient.dentalHistory.dentalCleaning = textfieldExam.isEmpty ? "N/A" : textfieldExam.text!

    }
    
    func loadValues()  {
        textviewReason.text = patient.dentalHistory.dentalReason == "" ? "PLEASE TYPE HERE" : patient.dentalHistory.dentalReason
        if textviewReason.text == "PLEASE TYPE HERE"{
            textviewReason.textColor = UIColor.lightGray
        }else{
            textviewReason.textColor = UIColor.black
        }
        radioToothBrush.setSelectedWithTag(patient.dentalHistory.toothBrushTag)
        textfieldDentalCheckup.setSavedText(patient.dentalHistory.dentalCheckup)
        textfieldXray.setSavedText(patient.dentalHistory.dentalXray)
        textfieldExam.setSavedText(patient.dentalHistory.dentalCleaning)
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
        ///self.navigationController?.popViewControllerAnimated(true)
    }


    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textviewReason.text == "PLEASE TYPE HERE" {
            let alert = Extention.alert("PLEASE ENTER ALL THE REQUIRED FIELDS")
            self.present(alert, animated: true, completion: nil)
        }else{
        saveValues()
        let dental = medicalStoryboard.instantiateViewController(withIdentifier: "Dental2VC") as! DentalHistory2ViewController
        dental.patient = self.patient
        self.navigationController?.pushViewController(dental, animated: true)
        }
    }

}

extension DentalHistory1ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}

extension DentalHistory1ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

