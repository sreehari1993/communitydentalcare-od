//
//  DentalHistory3ViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistory3ViewController: PDViewController {
    
    @IBOutlet weak var dropDownBrush : BRDropDown!
    @IBOutlet weak var dropDownFloss : BRDropDown!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelSignature : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropDownBrush.items = ["ONCE A DAY","TWICE A DAY","THRICE A DAY","I DON'T BRUSH"]
        dropDownFloss.items = ["ONCE A WEEK","TWICE A WEEK","THRICE A WEEK","I DON'T FLOSS"]
        labelDate1.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        loadValues()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func saveValues (){
        patient.dentalHistory.brush = dropDownBrush.selectedOption == nil ? "-- SELECT --" : dropDownBrush.selectedOption!
        patient.dentalHistory.floss = dropDownFloss.selectedOption == nil ? "-- SELECT --" : dropDownFloss.selectedOption!
        patient.dentalHistory.patientComments = textviewComments.text! == "PLEASE TYPE HERE" ? "N/A" : textviewComments.text!

    }
    
    func loadValues()  {
        textviewComments.text = (patient.dentalHistory.patientComments == "" || patient.dentalHistory.patientComments == "N/A") ? "PLEASE TYPE HERE" : patient.dentalHistory.patientComments
        if textviewComments.text == "PLEASE TYPE HERE"{
            textviewComments.textColor = UIColor.lightGray
        }else{
            textviewComments.textColor = UIColor.black
        }
        dropDownFloss.setSelectedOption = patient.dentalHistory.floss
        dropDownBrush.setSelectedOption = patient.dentalHistory.brush
        
    }
    
    @IBAction override func buttonActionBack(withSender sender : UIButton) {
        saveValues()
        super.buttonActionBack(withSender: sender)
    }

    
    @IBAction func onMinorButtonPressed (withSender sender : RadioButton){
        if sender.tag == 1{
            labelSignature.text = "Parent/Guardian Signature"
            labelPatientName.text = ""
        }else{
            labelSignature.text = "Signature"
            labelPatientName.text = patient.fullName

        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
         if !signaturePatient.isSigned() {
            let alert = Extention.alert("PLEASE SIGN THE FORM")
            self.present(alert, animated: true, completion: nil)
        }else if !labelDate1.dateTapped {
            let alert = Extention.alert("PLEASE SELECT THE DATE")
            self.present(alert, animated: true, completion: nil)
        }else{
            saveValues()
            patient.dentalHistory.signPatient = signaturePatient.signatureImage()
            let dental = medicalStoryboard.instantiateViewController(withIdentifier: "DentalFormVC") as! DentalHistoryFormViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)
        }
        
    }
    
}

extension DentalHistory3ViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE TYPE HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE TYPE HERE"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
