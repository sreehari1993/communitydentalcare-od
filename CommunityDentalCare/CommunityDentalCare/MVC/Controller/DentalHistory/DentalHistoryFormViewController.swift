//
//  DentalHistoryFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/28/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class DentalHistoryFormViewController: PDViewController {
    @IBOutlet weak var labelReason : UILabel!
    @IBOutlet      var arrayRadioButtons : [RadioButton]!
    @IBOutlet      var radioToothBrush : RadioButton!
    @IBOutlet weak var labelBrushTime : UILabel!
    @IBOutlet weak var labelFlossTime : UILabel!
    @IBOutlet weak var labelDentalCheckUp : UILabel!
    @IBOutlet weak var labelDentalXray : UILabel!
    @IBOutlet weak var labelDentalCleaning : UILabel!
    @IBOutlet weak var labelPopping : UILabel!
    @IBOutlet weak var textviewComments : UITextView!
    @IBOutlet      var labelOfficeComments : [UILabel]!
    @IBOutlet weak var labelDate1 : UILabel!
//    @IBOutlet weak var labelDate2 : UILabel!
//    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var labelDate4 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
//    @IBOutlet weak var signatureWitness : UIImageView!
//    @IBOutlet weak var signatureDentist : UIImageView!
    @IBOutlet weak var signatureOffice : UIImageView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDateOfBirth : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        labelReason.text = patient.dentalHistory.dentalReason
        for btn in arrayRadioButtons{
            btn.isSelected = patient.dentalHistory.dentalQuestions[btn.tag].selectedOption == true
        }
        radioToothBrush.setSelectedWithTag(patient.dentalHistory.toothBrushTag)
        labelBrushTime.text = patient.dentalHistory.brush == "-- SELECT --" ? "N/A" : patient.dentalHistory.brush
        labelFlossTime.text = patient.dentalHistory.floss == "-- SELECT --" ? "N/A" : patient.dentalHistory.floss
        labelDentalCheckUp.text = patient.dentalHistory.dentalCheckup
        labelDentalXray.text = patient.dentalHistory.dentalXray
        labelDentalCleaning.text = patient.dentalHistory.dentalCleaning
        labelPopping.text = patient.dentalHistory.dentalQuestions[12].answer
        textviewComments.text = patient.dentalHistory.patientComments
        patient.dentalHistory.dentalHistoryOfficeComments.setTextForArrayOfLabels(labelOfficeComments)
        labelDate1.text = patient.dateToday
//        labelDate2.text = patient.dateToday
//        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dentalHistory.dentalHistoryOfficeComments == "" ? "" : patient.dateToday
        signaturePatient.image = patient.dentalHistory.signPatient
//        signatureWitness.image = patient.dentalHistory.signWitness
//        signatureDentist.image = patient.dentalHistory.signDentist
        signatureOffice.image = patient.dentalHistory.dentalHistoryOfficeSign
        labelPatientName.text = patient.fullName
        labelDateOfBirth.text = patient.dateOfBirth
        
    }
    
    @IBAction override func buttonActionSubmit(withSender sender : UIButton) {
  
         super.sendFormToServer()
    }

}
