//
//  PDViewController.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

let medicalStoryboard = UIStoryboard(name: "MedicalHistory", bundle: Bundle.main)
let childMedicalStoryboard = UIStoryboard(name: "ChildMedicalHistory", bundle: Bundle.main)
let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
let patientStoryboard = UIStoryboard(name : "Patient", bundle: Bundle.main)

class PDViewController: UIViewController {
    
    var patient : PDPatient!
        var completion: ((_ success: Bool) -> Void)?
    @IBOutlet weak var pdfView: UIScrollView?
    
    
    @IBOutlet weak var buttonBack: UIButton?
    @IBOutlet weak var buttonSubmit: UIButton?
    var isFromPatientInfo: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        buttonSubmit?.backgroundColor = UIColor.green

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    var isFromPreviousForm: Bool {
        get {
            if navigationController?.viewControllers.count > 3 && navigationController?.viewControllers[2].isKind(of: VerificationViewController.self) == true {
                    navigationController?.viewControllers.remove(at: 2)
                    return false
            }
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonActionBack(withSender sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func buttonBackAction(sender: AnyObject) {
//        buttonActionBack(sender)
//    }
//    
    
    func showAlert2(_ message: String) {
        let alert = Extention.alert(message)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert1(_ message: String, completion: @escaping (_ completed: Bool) -> Void) {
        let alertController = UIAlertController(title: "Community Dental Care", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion(true)
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func sendFormToGoogleDrive(_ completion: @escaping (_ success: Bool) -> Void) {
        //      self.completion = completion
        //                self.buttonActionSubmit(nil)
        self.uploadPdfToDrive()
        
    }
    
    func sendFormToServer() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            return
        }
        var image: UIImage?
        self.buttonSubmit?.isHidden = true
        self.buttonBack?.isHidden = true
        self.pdfView?.isScrollEnabled = false
        self.pdfView?.clipsToBounds = false
        let size: CGSize = CGSize(width: self.pdfView!.contentSize.width, height: self.pdfView!.contentSize.height)
        
        let savedContentOffset = self.pdfView!.contentOffset
        UIGraphicsBeginImageContext(size)
        self.pdfView?.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        self.pdfView?.contentOffset = savedContentOffset
        UIGraphicsEndImageContext()
        
        func resetValues() {
            self.buttonSubmit?.isHidden = false
            self.buttonBack?.isHidden = false
            self.pdfView?.isScrollEnabled = true
            self.pdfView?.clipsToBounds = true
            BRProgressHUD.hide()
        }
        
        
        if let imageData = image {
            BRProgressHUD.show()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let birthDate = dateFormatter.date(from: patient.dateOfBirth)
            dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
            let birthDateString = dateFormatter.string(from: birthDate!)
            
            
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: Date()).uppercased()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            
        //    #if AUTO
                let params = ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateString, "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName, "patient_id": patient.patientDetails!.patientNumber, "dob": birthDateString]
//            #else
//                let params = ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateString, "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName]
//            #endif
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: serverPath))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            //http://opcommunity.mncell.com/formreviewapp/upload_medical_history_form.php?first_name=&last_name=&doc_date=&form_name=&client_id=&file_name=
            manager.post("upload_medical_history_form.php", parameters: params, constructingBodyWith: { (data) in
                dateFormatter.dateFormat = "MMddyyyyHHmmSSS"
                let randomString = dateFormatter.string(from: Date()).uppercased()
                let fileName = "\(self.patient.fullName.fileName)_\(dateString)_\(self.patient.selectedForms.first!.formTitle.fileName)_\(randomString).jpg"
                data.appendPart(withFileData: UIImageJPEGRepresentation(imageData, 1.0)!, name: self.patient.selectedForms.first!.formTitle.fileName, fileName: fileName, mimeType: "image/jpeg")
            }, progress: { (progress) in
                print("progressing...")
            }, success: { (task, result) in
                print("succss...")
                resetValues()
                self.patient.selectedForms.removeFirst()
                self.gotoNextForm(false)
            }) { (task, error) in
                print("errrooorrrr...")
                resetValues()
                self.showAlert(error.localizedDescription)
            }
        } else {
            self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            resetValues()
        }
    }

    
    @IBAction func buttonActionSubmit(withSender sender : UIButton) {
        self.buttonSubmit?.isUserInteractionEnabled = false
        self.buttonBack?.isUserInteractionEnabled = false
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Wellness Dental", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.present(alertController, animated: true, completion: nil)
            self.buttonSubmit?.isUserInteractionEnabled = true
            self.buttonBack?.isUserInteractionEnabled = true
            return
        }
        uploadPdfToDrive()
    }
    
    
    func uploadPdfToDrive (){
        
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self
        ) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                if let _ = self.pdfView {
                    if self.pdfView!.isKind(of: UIScrollView.self) {
                        pdfManager.createPDFForScrollView(self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                            if finished {
                                if let _ = self.patient.pendingForm {
                                    self.completion?(true)
                                } else {
                                    self.patient.selectedForms.removeFirst()
                                    self.gotoNextForm(false)
                                }
                            } else {
                                self.buttonSubmit?.isHidden = false
                                self.buttonBack?.isHidden = false
                                self.buttonSubmit?.isUserInteractionEnabled = true
                                self.buttonBack?.isUserInteractionEnabled = true
                                self.completion?(false)
                            }
                        })
                    }
                } else {
                    pdfManager.createPDFForView(self.view, patient: self.patient, completionBlock: { (finished) -> Void in
                        if finished {
                            if let _ = self.patient.pendingForm {
                                self.completion?(true)
                            } else {
                                self.patient.selectedForms.removeFirst()
                                self.gotoNextForm(false)
                            }
                        } else {
                            self.buttonSubmit?.isHidden = false
                            self.buttonBack?.isHidden = false
                            self.buttonSubmit?.isUserInteractionEnabled = true
                            self.buttonBack?.isUserInteractionEnabled = true
                            self.completion?(false)
                        }
                    })
                }
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
                self.buttonSubmit?.isUserInteractionEnabled = true
                self.buttonBack?.isUserInteractionEnabled = true
                self.completion?(false)
            }
        }
        
    }
    
    func gotoNextForm(_ isFromPatientInfoValue: Bool) {
        if isFromPreviousForm {
            if self.navigationController?.viewControllers.count > 2 {
                self.navigationController?.viewControllers.removeSubrange(2...(self.navigationController?.viewControllers.count)! - 2)
            }
        }

    
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        if formNames.contains(kNewPatientSignInForm) || formNames.contains(kPatientSignInForm){
            let patientSignIn = patientStoryboard.instantiateViewController(withIdentifier: "PatientRegistrationStep1Vc") as! PatientRegistrationStep1Vc
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }else if formNames.contains(kMedicalHistoryForm){
            let medical = medicalStoryboard.instantiateViewController(withIdentifier: "Medical1VC") as! MedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }else if formNames.contains(kDentalHistory){
                let dental = medicalStoryboard.instantiateViewController(withIdentifier: "Dental1VC") as! DentalHistory1ViewController
                dental.patient = self.patient
                self.navigationController?.pushViewController(dental, animated: true)
        }else if formNames.contains(kChildMedicalHistory){
            let medical = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildMedical1VC") as! ChildMedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }else if formNames.contains(kChildDentalHistory){
            let dental = childMedicalStoryboard.instantiateViewController(withIdentifier: "ChildDental1VC") as! ChildDentalHistory1ViewController
            dental.patient = self.patient
            self.navigationController?.pushViewController(dental, animated: true)

        }else if formNames.contains(kInsuranceCard) {
            let cardCapture = mainStoryboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let cardCapture = mainStoryboard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            cardCapture.isFromPatientInfo = isFromPatientInfoValue
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.contains(kNoticeOfPrivacy){
            let privacy = mainStoryboard.instantiateViewController(withIdentifier: "Privacy1VC") as! PrivacyPractice1ViewController
            privacy.patient = self.patient
            self.navigationController?.pushViewController(privacy, animated: true)
         }else if formNames.contains(kInformedConsent){
            let informed = mainStoryboard.instantiateViewController(withIdentifier: "InformedConsent1VC") as! InformedConsent1ViewController
            informed.patient = self.patient
            self.navigationController?.pushViewController(informed, animated: true)

         }else if formNames.contains(kFeedBack) {
            let feedBackVC = mainStoryboard.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
            feedBackVC.patient = self.patient
            self.navigationController?.pushViewController(feedBackVC, animated: true)
        }
        else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
        
    }
}

