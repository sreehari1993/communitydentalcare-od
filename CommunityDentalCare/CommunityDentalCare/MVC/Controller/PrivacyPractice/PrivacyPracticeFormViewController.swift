//
//  PrivacyPracticeFormViewController.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/26/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class PrivacyPracticeFormViewController: PDViewController {
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelPhoneNumber : UILabel!
    @IBOutlet weak var representativeName : UILabel!
    @IBOutlet weak var representativeRelationship : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var signature : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelAddress.text = patient.privacyAddress + ", " + patient.privacyCity + ", " + patient.privacyState + ", " + patient.privacyZipcode
        labelPhoneNumber.text = patient.privacyPhoneNumber
        representativeName.text = patient.privacyRepresentativeName
        representativeRelationship.text = patient.privacyRepresentativeRelationship
        labelDate.text = patient.dateToday
        signature.image = patient.privacySignature
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
