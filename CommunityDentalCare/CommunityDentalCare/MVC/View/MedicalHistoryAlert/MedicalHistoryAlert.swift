//
//  MedicalHistoryAlert.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryAlert: UIView {

    @IBOutlet var textviewComments : UITextView!
    @IBOutlet var signatureOffice : SignatureView!
    @IBOutlet var labelDate : DateLabel!
    
    var completion:((String,UIImage)->Void)?
    var errorAlert : ((String) -> Void)?

    static var sharedInstance : MedicalHistoryAlert {
        let instance :  MedicalHistoryAlert = Bundle.main.loadNibNamed("MedicalHistoryAlert", owner: nil, options: nil)!.first as! MedicalHistoryAlert
        return instance
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "PLEASE ENTER COMMENTS HERE" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "PLEASE ENTER COMMENTS HERE"
            textView.textColor = UIColor.lightGray
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }

    func showPopUp(_ inview : UIView, date : String, completion : @escaping (_ comments : String , _ sign : UIImage) -> Void , showAlert : @escaping (_ alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        textviewComments.text = "PLEASE ENTER COMMENTS HERE"
        labelDate.todayDate = date
        signatureOffice.clear()
        self.frame = screenSize
        
        inview.addSubview(self)
        inview.bringSubview(toFront: self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func onCancelButtonPressed (withSender sender : UIButton){
        self.completion!("", UIImage())
        self.removeFromSuperview()

    }
    
    @IBAction func okButtonPressed (withSender sender : UIButton){
        if  textviewComments.isEmpty {
            self.errorAlert!("PLEASE ENTER THE COMMENTS")
        }else if !signatureOffice.isSigned(){
            self.errorAlert!("PLEASE SIGN THE FORM")

        }else if !labelDate.dateTapped{
            self.errorAlert!("PLEASE SELECT THE DATE")
        }
        else{
            self.completion!(textviewComments.text! == "PLEASE ENTER COMMENTS HERE" ? "" : textviewComments.text!, signatureOffice.signatureImage())
            self.removeFromSuperview()
        }
    }

}
