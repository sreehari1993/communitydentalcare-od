//
//  AllergyTableViewCell.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 10/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AllergyTableViewCell: UITableViewCell {
    
    @IBOutlet var labelAllergyName : UILabel!
    @IBOutlet var buttonSelection : UIButton!
    @IBOutlet var labelDescription : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ allgy : Allergy, patnt : PDPatient)  {
//        if let patientDetails = patnt.patientDetails {
//            let predicate : NSPredicate = NSPredicate(format: "allergyId == %@", allgy.allergyId)
//            let array = (patientDetails.allergySelected as NSArray).filteredArrayUsingPredicate(predicate)
//            if array.count > 0{
//                let allgySelected = array[0] as! Allergy
//                allgy.allergyDescription = allgySelected.allergyDescription
//                allgy.isSelected = true
//            }
//        }
        
        labelAllergyName.text = allgy.allergyName
        buttonSelection.isSelected = allgy.isSelected
        if let labl = labelDescription {
            if allgy.isSelected {
                labl.text = allgy.allergyDescription
            }
        }
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
        
        
    }
    
    
    
    func configureFormCell(_ allgy : Allergy)  {
        labelAllergyName.text = allgy.allergyName
        buttonSelection.isSelected = allgy.isSelected
        if let labl = labelDescription{
            if allgy.isSelected {
                labl.text = allgy.allergyDescription
            }
        }
    }
    
    
}
