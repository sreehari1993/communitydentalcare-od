//
//  MedicationAlert.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 9/27/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class MedicationAlert: UIView {
    
    @IBOutlet var textviewOther : UITextView!
    @IBOutlet var buttonCollection: [UIButton]!
    
    var arrayButtonTags : [Int] = [Int]()
    

    var completion:(([Int],String)->Void)?
    var errorAlert : ((String) -> Void)?

    static var sharedInstance : MedicationAlert {
        let instance :  MedicationAlert = Bundle.main.loadNibNamed("MedicationAlert", owner: nil, options: nil)!.first as! MedicationAlert
        return instance
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    
    func showPopUp(_ inview : UIView, patient: PDPatient, completion : @escaping (_ arrayTags : [Int] , _ other : String) -> Void , showAlert : @escaping (_ alertMessage : String)-> Void)  {
        self.completion = completion
        self.errorAlert = showAlert
        textviewOther.text = ""
        arrayButtonTags.removeAll()
        
        for button in self.buttonCollection {
            button.isSelected = false
        }
        
        if patient.medicalHistory.medicationsTag.count > 0 {
            arrayButtonTags.append(contentsOf: patient.medicalHistory.medicationsTag)
            for tag in patient.medicalHistory.medicationsTag {
                let button = self.buttonCollection.filter({ (button) -> Bool in
                    return button.tag == tag
                })
                button[0].isSelected = button.count > 0
                if tag == 5 {
                    if patient.medicalHistory.medicationOthers != "N/A" {
                        textviewOther.text = patient.medicalHistory.medicationOthers
                        textviewOther.isUserInteractionEnabled = true
                        textviewOther.alpha = 1.0
                    }
                }
            }
        }
        self.frame = screenSize
        
        inview.addSubview(self)
        inview.bringSubview(toFront: self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func medicationsButtonAction (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if arrayButtonTags.contains(sender.tag){
            arrayButtonTags.remove(at: arrayButtonTags.index(of: sender.tag)!)
        }else{
            arrayButtonTags.append(sender.tag)
        }
        
        if sender.tag == 5 && sender.isSelected == true{
            textviewOther.isUserInteractionEnabled = true
            textviewOther.alpha = 1.0
        }else if sender.tag == 5 && sender.isSelected == false{
            textviewOther.text = ""
            textviewOther.isUserInteractionEnabled = false
            textviewOther.alpha = 0.5
        }
    }


    @IBAction func okButtonPressed (withSender sender : UIButton){
        if arrayButtonTags.contains(5) && textviewOther.isEmpty {
            self.errorAlert!("PLEASE SPECIFY THE OTHER MEDICATION")
        } else{
            self.completion!(arrayButtonTags,textviewOther.isEmpty ? "N/A" : textviewOther.text!)
            self.removeFromSuperview()
        }
    }

}
