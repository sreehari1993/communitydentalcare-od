//
//  Allergy.swift
//  DiamondDentalAuto
//
//  Created by Bala Murugan on 10/7/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Allergy: NSObject {
    
    var allergyId : String!
    var allergyName : String!
    var allergyDescription : String!
    var isSelected : Bool!

    
    init(details : NSDictionary) {
        super.init()
        allergyId = details ["AllergyId"] as? String
        allergyName = details ["AllergyName"] as? String
        allergyDescription = ""
        isSelected = false
    }
    
    override init() {
        super.init()
    }

    class func getAllData(_ arrayObjects : [NSDictionary]?) -> [Allergy]? {
        guard let arrayObjects = arrayObjects,  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var allgys = [Allergy]()
        for dict in arrayObjects {
            let allgy = Allergy(details: dict)
            allgys.append(allgy)
        }
        return allgys
    }

    class func getAllergyList( _ success:@escaping (_ result : [Allergy]) -> Void, failure :@escaping (_ error : Error?) -> Void) {
        ServiceManager.fetchDataFromService(hostUrl, serviceName: "consent_fetch_existing_allergy.php", parameters: nil, success: { (result) in
            let response : NSDictionary = result as! NSDictionary
            if (response["status"] as! String) == "SUCCESS" {
                let allgy = Allergy.getAllData(response["allergyArray"] as? [NSDictionary])
                if let _ = allgy {
                    success(allgy!)
                } else {
                    failure(NSError(errorMessage: "No data found") as Error)
                }
            }else{
                failure(NSError(errorMessage: "No data found") as Error)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    class func addAllergyList(_ params : NSDictionary, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error?) -> Void) {
        ServiceManager.fetchDataFromService(hostUrl, serviceName: "consent_add_update_existing_allergy.php", parameters: params , success: { (result) in
            let response : NSDictionary = result as! NSDictionary
            if (response["status"] as! String) == "SUCCESS" {
                success(response as NSDictionary)
            } else {
                failure(NSError(errorMessage: "No data found") as Error)
            }
        }) { (error) in
            failure(error)
        }
    }
    

}
