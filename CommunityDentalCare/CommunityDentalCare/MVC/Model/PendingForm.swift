//
//  PendingForm.swift
//  WestKendall
//
//  Created by Leojin Bose on 10/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

let serverPath = "https://opcommunity.mncell.com/formreviewapp/"

class PendingForm: NSObject {
    
    var fileId: String = ""
    var fileName: String = ""
    var formName: String = ""
    var fileUrl: NSURL?
    var uploadDate: String = ""
    var formImage: UIImage?
    var firstName: String!
    var lastName: String!
    var patientId: String = ""
    var dateOfBirth: String!
    
    init(details: [String: String]) {
        super.init()
        self.fileId = details["file_id"]!
        self.fileName = details["fie_name"]!
        self.formName = details["form_name"]!
        self.uploadDate = details["doc_date"]!
        self.firstName = details["first_name"]!
        self.lastName = details["last_name"]!
        self.fileUrl = NSURL(string: details["file_path"]!)
        patientId = getValue(value: details["patient_id"])
        dateOfBirth = getValue(value: details["dob"])
    }
    
    func getValue(value: Any?) -> String {
        if value is String {
            return value as! String
        }
        return ""
    }
    
    class func getArrayOfDocuments(documents: [[String: String]]) -> [PendingForm]! {
        var allDocuments = [PendingForm]()
        for document in documents {
            let doc = PendingForm(details: document)
            allDocuments.append(doc)
        }
//        #if AUTO
            let filtered = allDocuments.filter({ (pendingForm) -> Bool in
                return !pendingForm.patientId.isEmpty && pendingForm.patientId != "0"
            })
//        #else
//            let filtered = allDocuments.filter({ (pendingForm) -> Bool in
//                return pendingForm.patientId.isEmpty || pendingForm.patientId == "0"
//            })
//        #endif
        return filtered
    }
    
    class func staffLoginWithUsername(userName: String, password: String, completion: @escaping (_ success: Bool, _ result: [PendingForm]?, _ error: NSError?) -> Void) {
        //https://opcommunity.mncell.com/formreviewapp/staff_login.php?username=&password=&client_id=&
        ServiceManager.fetchDataFromService(serverPath, serviceName: "staff_login.php", parameters: ["username": userName, "password": password, "client_id": kAppKey], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                guard let documents = (result["posts"] as AnyObject)["documentList"] as? [[String: String]], documents.count > 0 else {
                    completion(true, nil, nil)
                    return
                }
                let allDocuments = self.getArrayOfDocuments(documents: documents)
                completion(true, allDocuments, nil)
            } else {
                completion(false, nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil, error as NSError)
        }
    }
    
    func submitCompletedForm(completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //http://opcommunity.mncell.com/formreviewapp/submit_medical_history_form.php?file_id=&client_id=&
        ServiceManager.fetchDataFromService(serverPath, serviceName: "submit_medical_history_form.php", parameters: ["file_id": self.fileId, "client_id": kAppKey], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, error as NSError)
        }
    }
}
