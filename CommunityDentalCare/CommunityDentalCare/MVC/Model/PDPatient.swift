//
//  PDPatient.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDPatient: NSObject {
    
    var fullName: String! {
        get {
            return initial.characters.count == 0 ?  firstName + " " + lastName : firstName + " " + initial + " " + lastName
        }
    }
    var patientDetails : PatientDetails?
    
    //ALLERGY
    var allergyArray : [Allergy] = [Allergy]()
 var pendingForm: PendingForm?
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String = ""
    var dateToday : String!
    var dateOfBirth : String!
    
    var medCommentsValue : String!
    var dentistInitials: UIImage!
    
    var address : String = ""
    var city : String = ""
    var state : String = ""
    var zipcode : String = ""
    var cellPhoneNumber : String = ""

    //visitorcheckin
    var VisitorFullName : String!
    var VisitingPurpose : String!
    
    //PRIVACY PRACTICE
    var privacyAddress : String = ""
    var privacyCity : String = ""
    var privacyState : String = ""
    var privacyZipcode : String = ""
    var privacyPhoneNumber : String = ""
    var privacyRepresentativeName : String = ""
    var privacyRepresentativeRelationship : String = ""
    var privacySignature : UIImage!
    
    //INFORMED CONSENT
    var informedConsentTechniqueTag : [Int]! = [Int]()
    var informedSignatureDoctor : UIImage!
    var informedSignatureParent : UIImage!
    var informedSignatureWitness : UIImage!
    
    var medicalHistory : MedicalHistory = MedicalHistory()
    var dentalHistory : DentalHistory = DentalHistory()
    var childMedicalHistory : ChildMedicalHistory = ChildMedicalHistory()
    var childDentalHistory : ChildDentalHistory = ChildDentalHistory()


    
    //PatientSignInForm
    var relation: String!
    var guardian2: GuardianPage2 = GuardianPage2()

    var preferredMethod : Int!
    var languageDropDown : Int!
    var raceDropDown : Int!
    var ethnicityDropDown : Int!
    var referralDropDown : Int!
    var languageOther : String = ""
    var raceOther: String = ""
    var ethnicityOther : String = ""
    var referredOther: String = ""
    
    var arrayLanguage: String!
    var arrayLanguageOther : String!
    var arrayrace: String!
    var arrayraceOther : String!
    var arrayethnicity: String!
    var arrayethnicityOther: String!
    var arrayreferral: String!
    var arrayreferralOther: String!

    
    var dentistName: String!
    var radiobuttonTag : Int!
    var radiobuttonTag2 : Int!
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String?
    var parentPhone : String!
    var parentAddress : String!
    var parentRadioButton : Int!
    
    var parentRelationType : String = ""
    var parentRelationType2 : Int!
    var isguardian : Bool!
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let yearVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: dateFormatter.date(from: self.dateOfBirth)!)
            let monthVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.month, from: dateFormatter.date(from: self.dateOfBirth)!)
            let dayVariation = (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: Date())  - (Calendar.current as NSCalendar).component(NSCalendar.Unit.day, from: dateFormatter.date(from: self.dateOfBirth)!)
            
            return monthVariation > 0 ? yearVariation - 1 : (monthVariation == 0 ? (dayVariation >= 0 ? yearVariation - 1 : yearVariation) : yearVariation)
        }
    }
    
    
    var zip: String = ""
    var phone: String = ""
    var socialSecurityNumber: String!
    var workPhone: String = ""
    var cellPhone: String = ""
    var email: String = ""
    
    var maritalStatus: Int!
    var maritalStatusText : String!
    var gender: Int!
    var isEmployed: Bool!
    var isResponsibleParty: Bool!
    var havePrimaryInsurance: Bool!
    var haveSecondaryInsurance: Bool!
    
    var employerName: String!
    var employerPhone: String!
    
    var emergencyFirstName: String = ""
    var emergencyLastName: String = ""
    
    var emergencyPhone: String = ""
    var emergencyRelation : String = ""
    var emergencyRelationOther : String = ""
    var emerRelation : Int!
    
    var responsibleRelation: Int!
    var responsibleName: String!
    var responsibleAddress: String!
    var responsibleCity: String!
    var responsibleState: String!
    var responsibleZip: String!
    var responsibleGroup: String!
    var responsibleOtherRelation: String!
    var signatureResponsible: UIImage!
    
    var primaryInsurance: PrimaryInsurance = PrimaryInsurance()
    var secondaryInsurance: SecondaryInsurance = SecondaryInsurance()
    
    var witnessName : String!
    var witnessPhone : String!
    var signature : UIImage!
    
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
    
    var signatureFinancialPolicy: UIImage!
    var signatureAppointmentPolicy: UIImage!
    var signatureReminderPolicy: UIImage!
    
    var reminderPhoneNumber: String!
    var reminderEmail: String!
    var isAllowText: Bool!
    var isAllowEmail: Bool!

    

    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
}

class PrimaryInsurance: NSObject {
    var policyholderName: String = ""
    var insuredPlanName: String = ""
    var address: String = ""
    var groupId: String = ""
    var policyId: String = ""
    var phone: String = ""
    var dateOfBirth: String = ""
}
class SecondaryInsurance: NSObject {
    var policyholderName: String = ""
    var insuredPlanName: String = ""
    var address: String = ""
    var groupId: String = ""
    var policyId: String = ""
    var phone: String = ""
    var dateOfBirth: String = ""
}


class GuardianPage2: NSObject {
    
    var firstName: String = ""
    var lastName: String = ""
    var address: String = ""
    var phoneNumber: String = ""
    var dateOfBirth : String!
    var relation: String!
    var relationtype2 : Int!
    var buttonIndex : Int = 3
    
    
}

class MedicalHistory : NSObject{
    var physicianName : String = ""
    var physicianAddress : String = ""
    var PhysicianPhone : String = ""
    var lastDentalExam : String = ""
    var bloodpressureLow : String = ""
    var bloodpressureHigh : String = ""
    var pulse : String = ""
    
    var medicalQuestions : [[PDQuestion]]!
    var medicationsTag : [Int] = [Int]()
    var medicationOthers : String = "N/A"
    
//    var arrayAllergyTags : [Int] = [Int]()
//    var allergyOthers : String = "N/A"
    var radioAllergyTag : Int = 0
    var arrayTestTags : [Int] = [Int]()
    var arraySmokeTag : [Int] = [Int]()
    var radioPregnantTag : Int = 0
    var radioWomanTag : Int = 2
    var radioBirthControlTag : Int = 0
    
    var patientComments : String = ""
    var patientSignature : UIImage!
    
    var medicalHistoryOfficeComments : String = ""
    var medicalHistoryOfficeSign : UIImage = UIImage()

    
    required override init() {
        super.init()
        let quest1: [String] = ["Do you have a specific medical condition that you want to discuss?",
                                "Are you under a physician's care or have you been hospitalized recently?",
                                "Have you had any surgeries or operations?",
                                "Are you taking medication?",
                                "Have you ever had heart problems, heart murmur or rheumatic fever?",
                                "Have you ever had a joint replacement?",
                                "Have you ever had head or neck surgery or radiation therapy?",
                                "Have you ever been told you need to take antibiotic premedication before dental treatment?",
                                "Have you ever had excessive bleeding after tooth extractions or cuts?",
                                "Have you ever had high or low blood pressure?",
                                "Do you have epilepsy, or ever feel faint, dizzy, or have seizures?",
                                "Have you ever been vaccinated for hepatitis B?",
                                "Are you a recovering alcoholic or drug abuser?"]
        
        let quest2: [String] = ["Diabetes Type I",
                                "Diabetes Type II",
                                "Thyroid problems",
                                "Liver problems",
                                "Kidney problems",
                                "Stomach ulcers",
                                "Bleeding problems",
                                "High cholesterol",
                                "Leukemia",
                                "Cancer",
                                "Asthma",
                                "Hay fever/bronchitis"]
        
        let quest3: [String] = ["Loss of hearing",
                                "Speech impairment",
                                "Eyesight problems",
                                "Mental illness",
                                "Mental retardation",
                                "Cerebral palsy",
                                "Cold sores",
                                "Sexually transmitted diseases",
                                "Arthritis",
                                "Birth defect",
                                "Pacemaker",
                                "Others conditions"]


        self.medicalQuestions = [PDQuestion.arrayOfQuestions(quest1),PDQuestion.arrayOfQuestions(quest2),PDQuestion.arrayOfQuestions(quest3)]
        self.medicalQuestions[0][2].isAnswerRequired = true
        self.medicalQuestions[0][3].isAnswerRequired = true
        self.medicalQuestions[0][9].isAnswerRequired = true
        self.medicalQuestions[2][11].isAnswerRequired = true

    }

}


class DentalHistory : NSObject{
    
    var dentalQuestions : [PDQuestion]!
    
    var dentalReason : String = ""
    var toothBrushTag : Int = 0
    var dentalCheckup : String = ""
    var dentalXray : String = ""
    var dentalCleaning : String = ""
    
    var brush : String = "-- SELECT --"
    var floss : String = "-- SELECT --"
    var patientComments : String = ""
    var signPatient : UIImage!
    var signWitness : UIImage!
    var signDentist : UIImage!
    
    var dentalHistoryOfficeComments : String = ""
    var dentalHistoryOfficeSign : UIImage = UIImage()


    required override init() {
        super.init()
        let quest1: [String] = ["Are you having any specific problems with your teeth, gums or mouth?",
                                "Are your teeth sensitive to hot, cold or sweets?",
                                "Do you have any fever blisters, mouth ulcers or sores on your lips or mouth?",
                                "Do you often have chapped lips, cracked or raw places in corners of your mouth?",
                                "Do your gums bleed after brushing or are they often sore or tender?",
                                "Do you have difficulty swallowing, chewing or do you frequently chew on one side only?",
                                "Do you frequently wedge food between your teeth?",
                                "Have you worn braces for straightening your teeth?",
                                "In general, do dental treatments cause you much concern or worry?",
                                "Do you chew or smoke tobacco in any form?",
                                "Are you dissatisfied with the appearance of your teeth?",
                                "Do you clench or grind your teeth?",
                                "Do you notice popping, clicking or soreness in the jaws or in front of your ears",
                                "Do you have frequent headaches, earaches or stiffness or soreness in your neck?"]
        
        self.dentalQuestions = PDQuestion.arrayOfQuestions(quest1)
        self.dentalQuestions[12].isAnswerRequired = true


    }
}

class ChildMedicalHistory : NSObject{
    
    var physicianName : String! = ""
    var physicianAddress : String = ""
    var PhysicianPhone : String = ""
    var bloodpressureLow : String = ""
    var bloodpressureHigh : String = ""
    var pulse : String = ""

    var medicalQuestions : [[PDQuestion]]!

    var allergyTag : Int = 0
    var arrayTestTags : [Int] = [Int]()
    
    var patientComments : String = "N/A"
    var patientSignature : UIImage!
    
    var childMedicalHistoryOfficeComments : String = ""
    var childMedicalHistoryOfficeSign : UIImage = UIImage()



    required override init() {
        super.init()
        let quest1: [String] = ["Does your child have a specific medical condition that you want to discuss?",
                                "Is your child under a physician's care or has he been hospitalized recently?",
                                "Is your child taking medication?",
                                "Has your child ever had a heart problem, heart murmur or rheumatic fever?",
                                "Has your child ever had excessive bleeding after tooth extractions or cuts?",
                                "Does your child ever feel faint, dizzy or have seizures?",
                                "Has your child been vaccinated for hepatitis B?"]
        
        let quest2: [String] = ["Eyesight Problems",
                                "Liver problems",
                                "Hay fever/bronchitis",
                                "Cancer",
                                "Cerebral palsy",
                                "Mental retardation",
                                "Speech impairment",
                                "Kidney problems",
                                "Thyroid problems",
                                "Infection",
                                "Pacemaker",
                                "Arthritis"]
        
        let quest3: [String] = ["Loss of hearing",
                                "Stomach ulcers",
                                "Bleeding problems",
                                "Diabetes",
                                "Herpes",
                                "Birth defect",
                                "Heart problems",
                                "Asthma",
                                "Leukemia",
                                "Epilepsy",
                                "Venereal disease"]
        
        
        self.medicalQuestions = [PDQuestion.arrayOfQuestions(quest1),PDQuestion.arrayOfQuestions(quest2),PDQuestion.arrayOfQuestions(quest3)]

    }
}

class ChildDentalHistory: NSObject {
    
    var medicalQuestions1 : [[PDQuestion]]!
    
    var reasonVisit : String = ""
    var treatment : String = ""
    var treatmentDate : String = ""
    var radioFearTag : Int = 0

    var patientComments : String = "N/A"
    var patientSignature : UIImage!

    var childDentalHistoryOfficeComments : String = ""
    var childDentalHistoryOfficeSign : UIImage = UIImage()

    required override init() {
        super.init()
        let quest1 : [String] = ["Is this your child's first time to visit a dentist?",
                                "Do you brush your child's teeth?",
                                "Does your child brush his/her own teeth?",
                                "Does your child suck his/her thumb or fingers?",
                                "Does your child eat candy or other sweets or drink soda pop",
                                "Has your child ever had a head injury?",
                                "Has your child ever had any injuries to his/her teeth, chin or jaws",
                                "Is your child afraid to visit the dentist?",
                                "Has anyone in your family ever had orthodontic treatment?",
                                "Does your child like the appearance of his/her teeth?",
                                "Would you like to discuss anything else with the dentist about your child's teeth?"]
        
        let quest2 : [String] = ["... putting your child to sleep with a bottle can cause tooth decay?",
                                 "... soda pop can cause your child to have tooth decay?",
                                 "... a child 1-4 years old needs an adult to brush and floss his teeth?",
                                 "... a child 5-7 years old needs an adult to supervise the brushing and flossing of his teeth?",
                                 "... you need to bring a child for a first visit to the dentist as soon as teeth erupt? (about 6-9 months)",
                                 "... you should bring your child to see the dentist every 6 months for check-up, cleaning, and fluoride treatments?",
                                 "... the dental professionals in our clinic care about your child's oral health?"]

        self.medicalQuestions1 = [PDQuestion.arrayOfQuestions(quest1), PDQuestion.arrayOfQuestions(quest2)]

    }
}

enum MaritalStatus: Int {
    case single = 0
    case married
    case child
    case widowed
    case divorced
}

enum Gender: Int {
    case male = 0
    case female
}

class PatientDetails : NSObject {
    var dateOfBirth : String!
    var firstName : String!
    var lastName : String!
    var preferredName : String!
    var address : String!
    var city : String!
    var state : String!
    var zipCode : String!
    var country : String!
    var gender : Int!
    var socialSecurityNumber : String!
    var email : String!
    var homePhone : String!
    var patientNumber : String!
    var maritalStatus: Int!
    var middleInitial : String!
    var medicalQuestions : [String]!
    var medications : [String]!
    var problems : [String]!

    var allergySelected : [Allergy] = [Allergy]()
    
    override init() {
        super.init()
    }
    
    init(details : [String: AnyObject]) {
        super.init()
        self.city = getValue(details["City"])
        self.dateOfBirth = getValue(details["Birthdate"])
        self.email = getValue(details["Email"])
        self.socialSecurityNumber = getValue(details["SSN"])
        self.address = getValue(details["Address"])
        self.zipCode = getValue(details["Zip"])
        self.gender = getValue(details["Gender"]) == "" ? nil : Int(getValue(details["Gender"]))
        self.firstName = getValue(details["FName"])
        self.patientNumber = getValue(details["PatNum"])
        self.state = getValue(details["State"])
        self.homePhone = getValue(details["HmPhone"])
        self.country = getValue(details["Country"])
        self.lastName = getValue(details["LName"])
        self.preferredName = getValue(details["Preferred"])
        self.maritalStatus = getValue(details["MaritalStatus"]) == "" ? nil : Int(getValue(details["MaritalStatus"]))
        self.middleInitial = getValue(details["MiddleI"])
//        
//        for dict in details["AllergyList"] as! [NSDictionary] {
//            let allgy: Allergy = Allergy()
//            allgy.allergyId = dict["AllergyId"] as! String
//            allgy.allergyDescription = ((dict["AllergyDescription"] as! String).uppercased() == "NO" || (dict["AllergyDescription"] as! String).uppercased() == "YES") ? "" : dict["AllergyDescription"] as! String
//            allgy.allergyName = dict["AllergyName"] as! String
//            allgy.isSelected = (dict["AllergyDescription"] as! String).uppercased() == "NO" ? false : true
//            self.allergySelected.append(allgy)
//        }
    }
    
    func getValue(_ value: AnyObject?) -> String {
        if value is String {
            return (value as! String).uppercased()
        }
        return ""
    }
}

