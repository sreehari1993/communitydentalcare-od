//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case `default` = 0
    case socialSecurity
    case toothNumber
    case phone
    case extensionCode
    case zipcode
    case number
    case date
    case month
    case year
    case dateInCurrentYear
    case dateIn1980
    case time
    case middleInitial
    case state
    case amount
    case email
     case username
}

//struct TextFormats: OptionSetType {
//    
//    let rawValue: UInt16
//    
//    init(rawValue: UInt16) {
//        self.rawValue = rawValue
//    }
//    
//    init(_ rawValue: UInt16) {
//        self.init(rawValue: rawValue)
//    }
//    
//    static let Default = TextFormats(1 << 0)
//    static let SocialSecurity = TextFormats(1 << 1)
//    static let ToothNumber = TextFormats(1 << 2)
//    static let Phone = TextFormats(1 << 3)
//    static let Zipcode = TextFormats(1 << 4)
//    static let Number = TextFormats(1 << 5)
//    static let Date = TextFormats(1 << 6)
//    static let DateOfBirth = TextFormats(1 << 7)
//    static let Time = TextFormats(1 << 8)
//    static let MiddleInitial = TextFormats(1 << 9)
//    static let State = TextFormats(1 << 10)
//    static let Month = TextFormats(1 << 11)
//    static let Year = TextFormats(1 << 12)
//    
//    static let AllDays = [Default, SocialSecurity, ToothNumber, Phone, Zipcode, Number, Date, DateOfBirth, Time, MiddleInitial, State, Month, Year]
//    
//    subscript(indexes: TextFormat...) -> [TextFormats] {
//        var weekdaySets = [TextFormats]()
//        
//        for i in indexes {
//            weekdaySets.append(TextFormats.AllDays[i.rawValue])
//        }
//        
//        return weekdaySets
//    }
//}
//
//func == (lhs: TextFormats, rhs: TextFormats) -> Bool {
//    return lhs.rawValue == rhs.rawValue
//}

//KEYS
//let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
//let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

//VARIABLES
//let kKeychainItemLoginName = "Master App: Google Login"
//let kKeychainItemName = "Master App: Google Drive"
//let kClientId = "704049274258-f9luqd6el3o1fr0gjfmd02m8i6r1dr62.apps.googleusercontent.com"
//let kClientSecret = "6tf9ErwOaXDNltGU4mJoZCwF"
//let kFolderName = "MasterClinic"
//
//let kDentistNames = ["DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME","DR. DENTIST NAME"]
//let kDentistNameNeededForms = [kNewPatientSignInForm]
//let kClinicName = "CLINIC NAME"
//let kAppName = "MASTER APP"
//let kPlace = "MINNEAPOLIS, MN"
//let kState = "MN"
//let kAppKey = "mcPolito"
//let kCommonDateFormat = "MMM dd, yyyy"
//
//let kGoogleID = "demo@srswebsolutions.com"
//let kGooglePassword = "Srsweb123#"
//let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
//let kVisitorCheckForm = "VISITOR CHECK IN FORM"
//let kNewPatientSignInForm = "NEW PATIENT SIGN-IN FORM"
//
//let kInsuranceCard = "SCAN INSURANCE CARD"
//let kDrivingLicense = "SCAN DRIVING LICENSE"
//
//let kFeedBack = "CUSTOMER REVIEW FORM"
//let kConsentForms = "CONSENT FORMS"
// END OF FORMS

//let toothNumberRequired: [String] = [""]
//Replace '""' with form names
//let consentIndex: Int = 4


