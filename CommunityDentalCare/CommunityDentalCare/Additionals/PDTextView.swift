//
//  PDTextView.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDTextView: UITextView {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    
    @IBInspectable var borderColor: UIColor = UIColor.black {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }


}


class TextView: PDTextView {
    
    @IBInspectable var clearOnEndEditing: Bool = true
    var placeHolder : String? = ""
    var isEdited : Bool {
        get {
            return (self.text != placeHolder && !self.isEmpty)
        }
    }
    override func awakeFromNib() {
        if placeHolder?.isEmpty == true {
            placeHolder = self.text
        }
        if self.delegate == nil {
            self.delegate = self
        }
    }
    
    func checkEdited() {
        if isEdited {
            self.textColor = UIColor.black
        } else {
            self.text = placeHolder
            self.textColor = UIColor.lightGray
        }
    }
    
}

extension TextView : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolder {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if clearOnEndEditing || textView.text.isEmpty  {
            textView.text = placeHolder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
