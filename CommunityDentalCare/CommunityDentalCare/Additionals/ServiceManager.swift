//
//  ServiceManager.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016 Advanced Aesthetic Dentistry. All rights reserved.
//

import UIKit

let kAppKey = "mcCommunityDental"

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : NSDictionary?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters!, progress: { (progress) in
            print(progress.fractionCompleted)
            }, success: { (task, result) in
                //                print(task)
                //                print(result)
                success(result as AnyObject)
        }) { (task, error) in
            //            print(task)
            //            print(error)
            failure(error)
        }
    }
    
//    class func fetchDataFromServiceCheckIn(serviceName: String, parameters : [String : String]?, success:(result : AnyObject) -> Void, failure :(error : NSError) -> Void) {
//        
//        let manager = AFHTTPSessionManager(baseURL: NSURL(string: "http://alpha.mncell.com/mconsent/"))
//        
//        manager.responseSerializer.acceptableContentTypes = ["text/html"]
//        manager.POST(serviceName, parameters: parameters, progress: { (progress) in
//            print(progress.fractionCompleted)
//            }, success: { (task, result) in
//                //                print(task)
//                //                print(result)
//                success(result: result!)
//        }) { (task, error) in
//            //            print(task)
//            //            print(error)
//            failure(error: error)
//        }
//    }
    
//    class func ChekInFormStatus(patientName: String, patientPurpose: String, completion: (success: Bool, error: NSError?) -> Void) {
//        //"mcdistinctivelaser"
//        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
//            if (result["posts"] as! String == "success") {
//                completion(success: true, error: nil)
//            } else {
//                completion(success: false, error: NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"]!)!["message"] as! String]))
//            }
//        }) { (error) in
//            completion(success: false, error: nil)
//        }
//    }
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, imageData: Data?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        
        manager.post(serviceName, parameters: parameters, constructingBodyWith: { (data) in
            if imageData != nil {
                data.appendPart(withFileData: imageData!, name: "task_data", fileName: "\(parameters!["patient_name"]!)_\(ServiceManager.date())_signature.jpeg", mimeType: "image/jpeg")
            }
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error)
        }
    }

    class func date() -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM_dd_yyyy_hh_mm"
        return dateFormat.string(from: Date())
    }
    
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, signatureImage: UIImage?, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromService("http://alpha.mncell.com/mconsent/", serviceName:"savepatients_info.php?", parameters: ["patientkey": "mcCommunityDental", "patientname": patientName, "patientpurpose": patientPurpose], imageData: signatureImage == nil ? nil : UIImageJPEGRepresentation(signatureImage!, 0.2), success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }

    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: Error?) -> Void) {
        //http://distinctdental.mncell.com/appservice/review.php
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": "mcCommunityDental", "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]) as Error)
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]) as Error)
        }
    }
    
    class func sendPatientDetails(_ patient: PDPatient, completion:@escaping (_ result : Bool, _ error: Error?) -> Void) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: patient.dateOfBirth)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        var params: [String: String] = [String: String]()
        params["Birthdate"] = dateFormatter.string(from: date!)
        params["FName"] = patient.firstName 
        params["LName"] = patient.lastName
        
        params["City"] = patient.city
        params["Email"] = patient.email
        params["Address"] = patient.address
        params["Zip"] = patient.zip
        
        params["HmPhone"] = patient.phone
        params["State"] = patient.state
        params["sex"] = "\(patient.gender)"
        params["marital_status"] = "\(patient.maritalStatus)"
        
        if let ssn = patient.socialSecurityNumber {
            params["SSN"] = ssn
        }
        if let patientDetails = patient.patientDetails {
            params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
        } else {
            params["PatNum"] = "0"
        }
        let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post("consent_form_add_newpatient_op.php", parameters: params, constructingBodyWith: { (formData) in

            }, progress: { (progress) in
                
            }, success: { (task, result) in
                if let patientId = (result as AnyObject)["patient_id"] as? Int, (result as AnyObject)["status"] as? String == "success" {
                    if let patientDetails = patient.patientDetails {
                        patientDetails.patientNumber = "\(patientId)"
//                        for dict in (result as AnyObject)["AllergyList"] as! [NSDictionary] {
//                            let allgy: Allergy = Allergy()
//                            allgy.allergyId = dict["AllergyId"] as! String
//                            allgy.allergyDescription = ((dict["AllergyDescription"] as! String).uppercased() == "NO" || (dict["AllergyDescription"] as! String).uppercased() == "YES") ? "" : dict["AllergyDescription"] as! String
//                            allgy.allergyName = dict["AllergyName"] as! String
//                            allgy.isSelected = (dict["AllergyDescription"] as! String).uppercased() == "NO" ? false : true
//                            patientDetails.allergySelected.append(allgy)
//                        }
                    } else {
                        patient.patientDetails = PatientDetails()
                        patient.patientDetails?.patientNumber = "\(patientId)"
                        for dict in (result as AnyObject)["AllergyList"] as! [NSDictionary] {
                            let allgy: Allergy = Allergy()
                            allgy.allergyId = dict["AllergyId"] as! String
                            allgy.allergyDescription = ((dict["AllergyDescription"] as! String).uppercased() == "NO" || (dict["AllergyDescription"] as! String).uppercased() == "YES") ? "" : dict["AllergyDescription"] as! String
                            allgy.allergyName = dict["AllergyName"] as! String
                            allgy.isSelected = (dict["AllergyDescription"] as! String).uppercased() == "NO" ? false : true
                            patient.patientDetails?.allergySelected.append(allgy)
                        }
                    }
                    completion(true, nil)
                } else {
                    completion(false, NSError(errorMessage: "SOMETHING WENT WRONG\nTRY AGAIN LATER") as Error)
                }
            }, failure: { (task, error) in
                completion(false, error)
        })
    }

    class func uploadFile(fileName: String, clientName: String, patientName: String, formName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post("consent_form_upload_to_server.php", parameters: nil, constructingBodyWith: { (formData) in
            formData.appendPart(withFileData: pdfData, name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
        }, progress: { (progress) in
            
        }, success: { (task, result) in
            
            
            
        }) { (task, error) in
            
        }
    }
}
