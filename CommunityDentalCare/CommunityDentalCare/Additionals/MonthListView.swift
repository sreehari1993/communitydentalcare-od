//
//  MonthListView.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MonthListView: UIView {
    var textField: UITextField!
    var pickerView: UIPickerView!
    var toolbar: UIToolbar!
    
    let dictMonth = ["JAN": "JANUARY",
                     "FEB": "FEBRUARY",
                     "MAR": "MARCH",
                     "APR": "APRIL",
                     "MAY": "MAY",
                     "JUN": "JUNE",
                     "JUL": "JULY",
                     "AUG": "AUGUST",
                     "SEP": "SEPTEMBER",
                     "OCT": "OCTOBER",
                     "NOV": "NOVEMBER",
                     "DEC": "DECEMBER"]
    let arrayKeys = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.showsSelectionIndicator = true
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        barbuttonDone.tintColor = UIColor.black
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(pickerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func donePressed() {
        textField.text = arrayKeys[pickerView.selectedRow(inComponent: 0)]
        textField.resignFirstResponder()
    }
    class func addMonthListForTextField(_ textField: UITextField) {
        let monthListView = MonthListView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.pickerView.reloadAllComponents()
        
        monthListView.textField = textField
    }
}
extension MonthListView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayKeys.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dictMonth[arrayKeys[row]]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textField.text = arrayKeys[row]
    }
}
