//
//  StateListView.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Leojin Bose on 4/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class StateListView: UIView {
    var textField: UITextField!
    var pickerView: UIPickerView!
    var toolbar: UIToolbar!
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        self.pickerView.showsSelectionIndicator = true
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
//        toolbar.translatesAutoresizingMaskIntoConstraints = false
//        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
        let buttonDone = UIButton(frame: CGRect(x: 0, y: 0, width: 80, height: 44))
        buttonDone.setTitle("Done", for: UIControlState())
        buttonDone.setTitleColor(UIColor.black, for: UIControlState())
        buttonDone.addTarget(self, action: #selector(StateListView.donePressed), for: UIControlEvents.touchUpInside)
        
//        buttonDone.translatesAutoresizingMaskIntoConstraints = false
        
        let barbuttonDone = UIBarButtonItem(customView: buttonDone)
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        let plist = Bundle.main.path(forResource: "USStateAbbreviations", ofType: "plist")
        let states = NSDictionary(contentsOfFile: plist!)
        arrayStates = states?.allKeys.sorted(by: { (obj1, obj2) -> Bool in
            let state1 = obj1 as! String
            let state2 = obj2 as! String
            return state1 < state2
        }) as! [String]
        
        self.addSubview(pickerView)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func donePressed() {
        let plist = Bundle.main.path(forResource: "USStateAbbreviations", ofType: "plist")
        let states = NSDictionary(contentsOfFile: plist!)
        textField.text = states?.value(forKey: arrayStates[pickerView.selectedRow(inComponent: 0)]) as? String
        textField.resignFirstResponder()
    }
    class func addStateListForTextField(_ textField: UITextField) {
        let stateListView = StateListView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = stateListView
        textField.inputAccessoryView = stateListView.toolbar
        stateListView.pickerView.reloadAllComponents()
        if textField.isEmpty {
            textField.text = "MN"
        }
        stateListView.textField = textField
    }
}
extension StateListView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayStates.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayStates[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let plist = Bundle.main.path(forResource: "USStateAbbreviations", ofType: "plist")
        let states = NSDictionary(contentsOfFile: plist!)
        textField.text = states?.value(forKey: arrayStates[pickerView.selectedRow(inComponent: 0)]) as? String
    }
}
