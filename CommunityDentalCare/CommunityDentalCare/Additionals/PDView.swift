//
//  PDView.swift
//  Advanced Aesthetic Dentistry
//
//  Created by Office on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDView: UIView {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true

    }
    
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

}
